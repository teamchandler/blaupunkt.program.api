FROM	node:0.12.0

# Bundle app source
COPY . /src
WORKDIR /src
# Install app dependencies
RUN npm install
EXPOSE  5000
CMD ["node", "app.js"]