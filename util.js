/**
 * Created by rahulguha on 22/02/14.
 */
var config =require('./config/config.json'),
    _ = require('lodash-node')
    ;
log4js = require('log4js');
var include_fieldjson = require('./config/include_field.json');

log4js.configure('./config/log4js.json', {});



exports.get_include_fieldjs = function(collection){
    var include_field =_.find(include_fieldjson.include_field, function(evt) {
        return evt.collection == collection;
    });
    return include_field.field;
}

exports.get_logger = function(log){
    var logger = log4js.getLogger(log);
    logger.setLevel(config.logging_level);
    return logger;
}

exports.get_listening_port = function(){
    return config.port;
}

exports.get_connection_string = function(){
    var e = config.env;
    var mongo_address = _.find(config.mongo_address, function(env) {
        return env.env == e;
    });
   // return  "mongodb://" + mongo_address.ip + "/" + config.db + "?poolSize=4";
     return  "mongodb://"+ config.mongo_user_id + ":" + config.mongo_password + "@" + address.ip + ":" +"27017" + "/" + config.db + "?poolSize=4" + "&authSource=admin";

}
exports.get_db_user = function(product){
    if (product == "mongo"){
        // todo
        return null;
    }
    else {
        return config.mysql.user_id;
    }
}
exports.get_db_pwd = function(product){
    if (product == "mongo"){
        // todo
        return null;
    }
    else {
        return config.mysql.password;
    }
}

exports.get_fetch_limit = function(){
    return config.fetch_limit;
}

exports.get_top_level_menu = function(){
    return config.top_level_menu;
}

exports.get_programs_details = function(){
    return config.programs;
}
exports.get_db = function(product){
    if (product == "mongo"){
        // todo
        return null;
    }
    else {
        return config.mysql.db;
    }
}
exports.get_mongo_host = function (){
    var e = config.env;
    var address = _.find(config.mongo_address, function(env) {
        return env.env == e;
    });
    return address.ip;
}
exports.get_mongo_host_db = function (){
    var e = config.env;
    var address = _.find(config.mongo_address, function(env) {
        return env.env == e;
    });
    return address.db;
}
exports.get_mongo_host_collection = function (){
    var e = config.env;
    var address = _.find(config.mongo_address, function(env) {
        return env.env == e;
    });
    return address.collection;
}
exports.get_mongo_host_auth_collection = function (){
    var e = config.env;
    var address = _.find(config.mongo_address, function(env) {
        return env.env == e;
    });
    return address.auth_collection;
}
exports.get_connection_string = function(product){
    var e = config.env;
    var connection_string ;
    if (product == "mongo"){
        var address = _.find(config.mongo_address, function(env) {
            return env.env == e;
             });
     //   connection_string = "mongodb://" + address.ip + "/" + address.db + "?poolSize=4";

     connection_string = "mongodb://"+ config.mongo_user_id + ":" + config.mongo_password + "@" + address.ip + ":" +"27017" + "/" + config.db + "?poolSize=4" + "&authSource=admin";
     }
    else { // for mysql
        var address = _.find(config.mysql.address, function(env) {
            return env.env == e;
        });
        connection_string = address;
    }
    return connection_string;
}
exports.get_connection_string_byip = function(product,ip){
    var e = "client";
    var connection_string ;
    if (product == "mongo"){
        var address = _.find(config.mongo_address, function(env) {
            return (env.env == e && env.ip==ip);
             });
        connection_string = "mongodb://" + address.ip + "/" + address.db + "?poolSize=4";
     }
    else { // for mysql
        var address = _.find(config.mysql.address, function(env) {
            return env.env == e;
        });
        connection_string = address;
    }
    return connection_string;
}
exports.get_logging_level = function(){
    return config.logging_level;
}
exports.get_email_info = function(){
    return config.email_info;
}
exports.get_email_templating_engine = function(engine){
    var engine_info = _.find(config.email_templating_engine, function(template_info) {
        return template_info.name == engine;
    });
    return engine_info;
}

function AWS (){
    var AWS_Key;
    var AWS_SECRET;
    var upload_link_expiry;
    var download_link_expiry;
    var upload_key;
    var bucket;
}

exports.get_aws = function ()
{
    var aws = new AWS();
    aws.AWS_KEY = config.AWS.key;
    aws.AWS_SECRET = config.AWS.secret;
    aws.upload_link_expiry = config.AWS.upload_link_expiry;
    aws.download_link_expiry = config.AWS.download_link_expiry;
    aws.upload_key = config.AWS.upload_key;
    aws.bucket = config.AWS.bucket;
    return aws;
}


exports.is_array= function(value)
{
    return Object.prototype.toString.call(value) === '[object Array]';
}

exports.get_program_name = function(){
    return config.program_name;
}


exports.calculate_points = function(total_sale, prog){

    try{

        var selected_rule = [];
        var p =  _.find(program_rule , function(program) {
            return program.program_code == prog;
        });

//    console.log('Rules');
//    console.log(p);

        total_sale = total_sale * 1;

        for (var i=0; i < p.rules.length; i++){
            if (((p.rules[i].min_val * 1) <= total_sale ) && ((p.rules[i].max_val * 1)>= total_sale ) ){
                selected_rule.push(p.rules[i].award);
                break;
            }
        }
//        console.log(prog);
//        console.log(selected_rule);
        return selected_rule;
    }
    catch (err){

        console.log('error');
        console.log(prog);
        console.log(total_sale);
        console.log(err);
    }


}

exports.create_routing_log = function (method, url, prefix, route)
{
    var tmp = "Route Sub System: " + route + "\n" + "Method: " + method + "\n" + "URL:" + "/" + prefix  + url;
    return tmp;
}

exports.get_source_by_query_purpose = function(query_purpose){
    var source = _.find(config.query_setup, function(q) {
        return (q.purpose == query_purpose);
    });
    return source;
}

exports.send_to_response = function(results, res ){
    if (results instanceof Array){
        var arr = [];
        results.forEach(function(r){
            arr.push(r)

        });
    }
    else{
        var arr = results;
    }
    res.contentType('application/json');
    res.send(arr);
}

exports.get_audit_trail_config = function(){
    return config.audit_trail;
}