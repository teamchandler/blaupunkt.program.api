/**
 * Created by rahulguha on 03/04/14.
 */
var menu = require('./repo/master/menu.js');


var top_menu ;

exports.menu = function(){
    return top_menu;
}

exports.get_menu = function(){
    menu.get_menu()
        .then(
        function (results) {
            if (results.length > 0) {
               top_menu = results;
            }

        },
        function (err) {
            res.send(err);
        }
    );
}