/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/2/14
 * Time: 6:16 AM
 * To change this template use File | Settings | File Templates.
 */
/* The API controller
 Exports 3 methods:
 * post - Creates a new thread
 * list - Returns a list of threads
 * show - Displays a thread and its posts
 */

//var fs = require ("fs");

var mysql = require("../../db_connect/mysql.js");
var util = require('../../util.js');
var token = require('../../token.js')

//var du = require('date-utils');
var cache = require('../../cache.js');
var shortid = require('shortid')
var aws = require('aws-sdk');
var knox = require('knox');
var Busboy = require('busboy');
//var pkgcloud = require('pkgcloud');
var fs = require('fs');
var path = require('path');

var async = require('async');
var _ = require("underscore");
//var inspect = require('util').inspect;


var claim = require('../../repo/master/claim');
var program = require('../../repo/master/program');
var logger = util.get_logger("api");
var region = require('../../repo/master/region');
var sku = require('../../repo/master/sku');
//var workflow = require('../model/workflow.js');
var user = require('../../repo/master/user');
var retailer = require('../../repo/master/retailer');

var sales_registry = require('../../repo/master/sales_registry');
var override_sku = require('../../repo/master/override_sku');
var user_points = require('../../repo/master/user_points');
var sku_tran = require('../../repo/master/sku_tran');
var sku_wise_point_history = require('../../repo/master/sku_wise_point_history');
var counter = require('../../repo/master/counter');



exports.get_retailer_list = function (req, res) {
    retailer.get_retailer_list()
        .then (
        function (results) {
            if (results.length > 0) {
                send_to_response(results, res);
            }
            else {
                send_empty_recordset(res);
            }
        },
        function (err) {
            res.send(err);
        }

    );
};

//********************  workflow  methods   *************************************

exports.get_workflow_by_company_region = function (req, res) {
    workflow.get_workflow_by_company_region(req.params.company, req.params.region)
        .then (
        function (results) {
            if (results.length > 0) {
                send_to_response(results, res);
            }
            else {
                send_empty_recordset(res);
            }
        },
        function (err) {
            res.send(err);
        }

    );
};
exports.get_sku_list_detailed = function (req, res) {
    sku.get_detailed_sku_list(req.params.company,req.params.region_id )
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.get_all_sku_master = function (req, res) {
    sku.get_all_sku_master()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


//******************** end  sku  methods   *************************************

//********************* Program Management ****************************************

exports.get_company_name = function (req, res) {
    claim.get_company_name()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_user_name_by_company= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    claim.get_user_name_by_company (req.params.name)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};








//**************************************************************************

//********************  sku  methods   *************************************

exports.get_sku_list = function (req, res) {
    sku.get_sku_list(req.params.company,req.params.region_id )
        .then (
        function (results) {
            if (results.length > 0) {
                send_to_response(results, res);
            }
            else {
                send_empty_recordset(res);
            }
        },
        function (err) {
            res.send(err);
        }

    );
};
exports.get_sku_list_detailed = function (req, res) {
    sku.get_detailed_sku_list(req.params.company,req.params.region_id )
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_monthly_sku_wise_sale = function (req, res) {
    sales_registry.get_all_skus()
        .then (
        function (results) {
            try {
               if (results.length > 0) {

                console.log ("********Execute Time : Total SKUs Form Blaupunkt ********* ");
                console.log (results);

                for (var i = 0; i < results.length; i++)
                {
                    results[i].qty_nov = 0;
                    results[i].qty_dec = 0;
                    results[i].qty_jan = 0;
                    results[i].qty_feb = 0;
                    results[i].qty_mar = 0;
                    results[i].qty_apr = 0;
                    results[i].qty_may = 0;
                    results[i].qty_june = 0;
                    results[i].qty_july = 0;
                    results[i].qty_august = 0;
                    results[i].qty_sep = 0;
                    results[i].qty_oct = 0;
                    results[i].qty_till_date = 0;
                    results[i].qty_today = 0;

                }



                //Get Already Credited Points For These Users
                async.map(results, get_nov_sku_qty, function (err, r) {

//                    console.log('For Month November');
//                    console.log(r);

                    for (var i = 0; i < r.length; i++) {
                        if(r[i][0]._id==null){
                            r[i][0].qty_nov=0;
                        }
                        if(results[i]._id==r[i][0]._id)
                        {
                            results[i].qty_nov = r[i][0].qty_nov;
                        }

                    }

                    //Get All User Wise Sales Information
                    async.map(results, get_dec_sku_qty, function (err, r) {

//                        console.log('For Month December');
//                        console.log(r);
                        for (var i = 0; i < r.length; i++) {
                            if(r[i][0]._id==null){
                                r[i][0].qty_dec=0;
                            }
                            if(results[i]._id==r[i][0]._id)
                            {
                                results[i].qty_dec = r[i][0].qty_dec;
                            }

                        }


                        async.map(results, get_jan_sku_qty, function (err, r) {
//                            console.log('For Month January');
//                            console.log(r);
                            for (var i = 0; i < r.length; i++) {
                                if(r[i][0]._id==null){
                                    r[i][0].qty_jan=0;
                                }
                                if(results[i]._id==r[i][0]._id)
                                {
                                    results[i].qty_jan = r[i][0].qty_jan;
                                }

                            }

                            async.map(results, get_feb_sku_qty, function (err, r) {
//                                console.log('For Month February');
//                                console.log(r);
                                for (var i = 0; i < r.length; i++) {
                                    if(r[i][0]._id==null){
                                        r[i][0].qty_feb=0;
                                    }
                                    if(results[i]._id==r[i][0]._id)
                                    {
                                        results[i].qty_feb = r[i][0].qty_feb;
                                    }

                                }

                                async.map(results, get_mar_sku_qty, function (err, r) {
//                                    console.log('For Month March');
//                                    console.log(r);
                                    for (var i = 0; i < r.length; i++) {
                                        if(r[i][0]._id==null){
                                            r[i][0].qty_mar=0;
                                        }
                                        if(results[i]._id==r[i][0]._id)
                                        {
                                            results[i].qty_mar = r[i][0].qty_mar;
                                        }

                                    }

                                    async.map(results, get_apr_sku_qty, function (err, r) {

//                                        console.log('For Month April');
//                                        console.log(r);

                                        for (var i = 0; i < r.length; i++) {
                                            if(r[i][0]._id==null){
                                                r[i][0].qty_apr=0;
                                            }
                                            if(results[i]._id==r[i][0]._id)
                                            {
                                                results[i].qty_apr = r[i][0].qty_apr;
                                            }

                                        }

                                        async.map(results, get_may_sku_qty, function (err, r) {

//                                            console.log('For Month May');
//                                            console.log(r);

                                            for (var i = 0; i < r.length; i++) {
                                                if(r[i][0]._id==null){
                                                    r[i][0].qty_may=0;
                                                }
                                                if(results[i]._id==r[i][0]._id)
                                                {
                                                    results[i].qty_may = r[i][0].qty_may;
                                                }

                                            }

                                            async.map(results, get_june_sku_qty, function (err, r) {

//                                                console.log('For Month June');
//                                                console.log(r);

                                                for (var i = 0; i < r.length; i++) {
                                                    if(r[i][0]._id==null){
                                                        r[i][0].qty_june=0;
                                                    }
                                                    if(results[i]._id==r[i][0]._id)
                                                    {
                                                        results[i].qty_june = r[i][0].qty_june;
                                                    }

                                                }

                                                async.map(results, get_july_sku_qty, function (err, r) {

//                                                    console.log('For Month July');
//                                                    console.log(r);


                                                    for (var i = 0; i < r.length; i++) {
                                                        if(r[i][0]._id==null){
                                                            r[i][0].qty_july=0;
                                                        }
                                                        if(results[i]._id==r[i][0]._id)
                                                        {
                                                            results[i].qty_july = r[i][0].qty_july;
                                                        }

                                                    }

                                                    async.map(results, get_august_sku_qty, function (err, r) {

//                                                        console.log('For Month August');
//                                                        console.log(r);

                                                        for (var i = 0; i < r.length; i++) {
                                                            if(r[i][0]._id==null){
                                                                r[i][0].qty_august=0;
                                                            }
                                                            if(results[i]._id==r[i][0]._id)
                                                            {
                                                                results[i].qty_august = r[i][0].qty_august;
                                                            }

                                                        }

                                                        async.map(results, get_sep_sku_qty, function (err, r) {

//                                                            console.log('For Month September');
//                                                            console.log(r);


                                                            for (var i = 0; i < r.length; i++) {
                                                                if(r[i][0]._id==null){
                                                                    r[i][0].qty_sep=0;
                                                                }
                                                                if(results[i]._id==r[i][0]._id)
                                                                {
                                                                    results[i].qty_sep = r[i][0].qty_sep;
                                                                }

                                                            }

                                                                async.map(results, get_oct_sku_qty, function (err, r) {

//                                                                    console.log('For Month October');
//                                                                    console.log(r);

                                                                    for (var i = 0; i < r.length; i++) {
                                                                        if(r[i][0]._id==null){
                                                                            r[i][0].qty_oct=0;
                                                                        }
                                                                        if(results[i]._id==r[i][0]._id)
                                                                        {
                                                                            results[i].qty_oct = r[i][0].qty_oct;
                                                                        }

                                                                    }

                                                                        async.map(results, get_today_sku_qty, function (err, r) {

//                                                                            console.log('For Today');
//                                                                            console.log(r);

                                                                            for (var i = 0; i < r.length; i++) {
                                                                                if(r[i][0]._id==null){
                                                                                    r[i][0].qty_today=0;
                                                                                }
                                                                                if(results[i]._id==r[i][0]._id)
                                                                                {
                                                                                    results[i].qty_today = r[i][0].qty_today;
                                                                                }

                                                                            }



                                                                            if(results.length>0){
                                                                                for (var i=0;i<results.length;i++)
                                                                                {
                                                                                    results[i].qty_till_date=(parseInt(results[i].qty_nov)+parseInt(results[i].qty_dec)+parseInt(results[i].qty_jan)+parseInt(results[i].qty_feb)+parseInt(results[i].qty_mar)+parseInt(results[i].qty_apr)+parseInt(results[i].qty_may)+parseInt(results[i].qty_june)+parseInt(results[i].qty_july)+parseInt(results[i].qty_august)+parseInt(results[i].qty_sep)+parseInt(results[i].qty_oct));
                                                                                }
                                                                            }


                                                                            console.log ("Execute Time : " + new Date() + " Total Calculated SKUs Form Blaupunkt : " + results.length);
                                                                            console.log ("********Execute Time : " + new Date() + " Total Calculated SKUs Form Blaupunkt ********* ");
                                                                            console.log (results);
                                                                            res.send(results);

                                                                 });

                                                            });

                                                        });

                                                    });

                                                });

                                            });

                                        });

                                    });

                                });

                            });

                        });

                    });

                });

            }
            }
            catch (e) {
                console.log("entering catch block");
                console.log(e);
                console.log("leaving catch block");
            }
            finally {
                console.log("entering and leaving the finally block");
            }
        },
        function (err) {
            res.send(err);
        });

};

var get_nov_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 11)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {
                var qty_nov = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_nov=qty_nov+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_nov" : qty_nov};
                out_results.push(first_val);

                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_nov" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_dec_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 12)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_dec = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_dec=qty_dec+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_dec" : qty_dec};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_dec" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_jan_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 1)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_jan = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_jan=qty_jan+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_jan" : qty_jan};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_jan" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_feb_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 2)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_feb = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_feb=qty_feb+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_feb" : qty_feb};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_feb" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_mar_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 3)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_mar = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_mar=qty_mar+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_mar" : qty_mar};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_mar" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_apr_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 4)
        .then (
        function (results) {

            //console.log("Data For April");


            var out_results=[];
            if (results.length > 0) {

                var qty_apr = 0;
              //  console.log(results);
                for (var i=0;i<results.length;i++)
                {
//                    console.log("Enter Loop");
//                    console.log(results[i].claim_details);
//                    console.log(r._id);
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
//                    console.log(obj_seleced_sku);
                    qty_apr=qty_apr+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_apr" : qty_apr};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_apr" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_may_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 5)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_may = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_may=qty_may+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_may" : qty_may};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_may" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_june_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 6)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_june = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_june=qty_june+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_june" : qty_june};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_june" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_july_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 7)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_july = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_july=qty_july+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_july" : qty_july};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_july" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_august_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 8)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_august = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_august=qty_august+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_august" : qty_august};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_august" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_sep_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 9)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_sep = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_sep=qty_sep+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_sep" : qty_sep};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_sep" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_oct_sku_qty = function(r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 10)
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_oct = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_oct=qty_oct+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_oct" : qty_oct};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_oct" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

var get_today_sku_qty = function(r, done_callback) {

    var cur_date = new Date();

    sales_registry.get_today_sku_qty(r._id, cur_date.getFullYear(),(cur_date.getMonth() + 1),cur_date.getDate())
        .then (
        function (results) {

            var out_results=[];
            if (results.length > 0) {

                var qty_today = 0;

                for (var i=0;i<results.length;i++)
                {
                    var obj_seleced_sku=  _.where(results[i].claim_details, {sku: r._id});
                    // console.log(obj_seleced_sku);
                    qty_today=qty_today+parseInt(obj_seleced_sku[0].quantity);
                }

                var first_val= {_id : r._id, "qty_today" : qty_today};
                out_results.push(first_val);
                return done_callback(null, out_results);

            }
            else
            {
                var first_val= {_id : null, "qty_today" : 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

exports.get_all_sku_master = function (req, res) {
    sku.get_all_sku_master()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


//******************** end  sku  methods   *************************************
//********************  claims  methods   *************************************
    exports.get_all_submissions = function (req, res) {
        claim.get_all_submissions (req.params.user, req.params.limit)
            .then (
            function (results){
                send_to_response(results, res);
            },
            function (err){
                res.send(err);
            }

        );
    };

    exports.get_total_submitted_value = function (req, res) {
        claim.get_total_submitted_value(req.params.user)
            .then (
            function (results){
                send_to_response(results, res);
            },
            function (err){
                res.send(err);
            }

        );
    };




   exports.get_details_by_user = function (req, res) {
    sales_registry.get_details_by_user(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_details_total_sale_by_user = function (req, res) {
    sales_registry.get_details_total_sale_by_user(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

//Added By Hassan Ahamed
exports.get_disti_wise_sales_data = function (req, res) {
    //Changed by Subhajit to suit the requirement on 29th may san do hazaar pandra

    var ids = [];
    var id ={i:0};
    ids.push(id);
    sku_wise_point_history.get_disti_wise_sales_data()
        .then (
        function (results){
            console.log(results);

            async.map(ids, get_total_amount_group_by_disti, function (err, r) {

                console.log(r[0][0]);

                var total_data = r[0][0];

            //    for(var i=0;i<results.length;i++){
                      if(total_data.length > 0){
                    _.each(results, function(element, index, list){
                        var disti_sku_data=_.where(total_data, {retailer: element.retailer});
                        console.log('inside async');
//                        console.log(disti_sku_data);
//                        console.log(disti_sku_data[0].total_sale);
                        if(disti_sku_data.length > 0)
                        {
                        console.log('inside async1');
                        element.current_month_sale = disti_sku_data[0].total_sale;
                        }
                        else
                        {
                        element.current_month_sale = 0;
                        }
                        console.log(element);
                        //_.extend(element.current_month_sale, disti_sku_data.total_sale);
                    });
                      }
//                  if(results[i].retailer == r[0][0].retailer)
//                  {
//                      console.log('inside if');
//                      results[i].current_month_sale = r[0][0].total_sale;
//
//                  }


             //   }

                console.log('After push');
                console.log(results);
                send_to_response(results, res);

            })


        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_disti_wise_sku_sales_data = function (req, res) {
    //sku_wise_point_history.get_disti_wise_sku_sales_data()


    sku_wise_point_history.get_disti_wise_sales_data()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

//End By Hassan Ahamed


exports.get_user_details_for_zone_address = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select * from user_registration where email_id = '" + req.params.user + "';";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
        });
};

exports.posting_goal_value = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "insert into ecomm.user_goal (email_id, goal, customized_goal) values ('" + req.body.user + "', '"+ req.body.goal + "', 0)";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);

        });
};


exports.updating_goal_value = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "update ecomm.user_goal set goal = '" + req.body.goal  + "' where email_id = '" + req.body.user +"'";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);

        });
};


exports.get_user_goal_value = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select * from user_goal where email_id = '" + req.params.user + "';";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
            //console.log(rows);
        });
};

exports.get_user_redeemed_points = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT sum(txn_amount) as redeemed FROM user_points where txn_type = 'd' and user_id = '" + req.params.user + "';";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
            //console.log(rows);
        });
};


exports.get_user_proceedings_invoice_current_month = function (req, res) {
    sales_registry.get_user_proceedings_invoice_current_month(req.params.user,req.params.stdate,req.params.endate)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_user_si_region_dashboard = function (req, res) {
    sales_registry.get_user_si_region_dashboard(req.params.regionName)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_user_si_allIndia_dashboard = function (req, res) {
    sales_registry.get_user_si_allIndia_dashboard()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};
//    exports.claims_by_company = function (req, res) {
//        //send_to_response([{"response": "from claim list"}], res);
//        claim.claims_by_company(req.params.company)
//            .then (
//                function (results){
//                    send_to_response(results, res);
//                },
//                function (err){
//                    res.send(err);
//                }
//
//        );
//    };
//
//    exports.claim_list = function (req, res) {
//
//        claim.claim_list()
//            .then (
//                function (results){
//                    send_to_response(results, res);
//                },
//                function (err){
//                    res.send(err);
//                }
//
//        );
//    };

    exports.add_claim = function (req, res) {
    var l_claim_id = util.get_program_name() + "/" + shortid.generate();
    var claim_data = {
        claim_id: l_claim_id
        , user_id: req.body.user_id
        , active : 1
        , invoice_date: new Date(req.body.invoice_date) //todo accept date here from caller
        , invoice_amount: req.body.invoice_amount
        , invoice_number: req.body.invoice_number
        , retailer: req.body.retailer
        , status : req.body.status
        , supporting_doc: req.body.supporting_doc
        , approval_comment: req.body.approval_comment
        , claim_details : []

    };

    var c = new claim(claim_data);

    c.save(function () {
        res.send('item saved');
    });
};


exports.update_sku_quantity = function (req, res) {

    for(var i=0;i<req.body.sku_quantity_details.length;i++) {

        var sku_data = {
            "sku": req.body.sku_quantity_details[i].sku,
            "frm_participant":req.body.sku_quantity_details[i].frm_participant ,
            "from_participant_code":req.body.sku_quantity_details[i].from_participant_code,
            "to_participant": req.body.sku_quantity_details[i].to_participant,
            "to_participant_code":req.body.sku_quantity_details[i].to_participant_code,
            "uploaded_by_participant_code":req.body.sku_quantity_details[i].uploaded_by_participant_code,
            "uploaded_by_participant":req.body.sku_quantity_details[i].uploaded_by_participant,
            "invoice_number":req.body.sku_quantity_details[i].invoice_number,
            "flow_type": 1,
            "tran_type": "d",
            "quantity": 0,
            "credit_quantity": 0,
            "debit_quantity": req.body.sku_quantity_details[i].debit_quantity,
            "active": 1,
            "created_date": new Date()
        };

        var c = new sku_tran(sku_data);

        c.save(function () {
            res.send('record saved successfully');
        });

    }
};

exports.add_override_sku = function (req, res) {

    var override_sku_data = {
         claim_id: req.body.claim_id
        , user_id: req.body.user_id
        , active : 1
        , sku: req.body.sku
        , quantity : req.body.quantity
        , created_date: new Date()
    };

    var c = new override_sku(override_sku_data);

    c.save(function () {
        res.send('record saved successfully');
    });
};


exports.add_sku_details = function (req, res) {

    var sku_detail_data = [];

    for(var i=0;i<req.body.length;i++)
    {

    var sku_details_data = {
          sku: req.body[i].sku
        , description: req.body[i].description
        , active : 1
        , model: req.body[i].model
        , mrp : req.body[i].mrp
        , bluerewards: req.body[i].bluerewards
    };

        var c = new sku(sku_details_data);

        c.save(function () {
            res.send('record saved successfully');
        });

       // sku_detail_data.push(sku_details_data);
    }



};






//**********Invoce Save Schneider***********//

function get_program_name(programs, invoce_date) {

    var prg_name="nrml";

    if(programs.length>0){
        for(var i=0;i<programs.length;i++){
          if (dateCheck(programs[i].start_date,programs[i].end_date,invoce_date)) {
              prg_name=programs[i].code;
          }
        }
    }

    return prg_name;
}


function save_part_code_details(data)
{
    if(data.length > 0)
    {

    for(var i=0;i<data.length;i++)
    {

        var sku_details_data = {
            sku: data[i].sku
            , description: data[i].description
            , active : 1
            , model: data[i].model
            , mrp : data[i].mrp
            , bluerewards: data[i].bluerewards
        };

        var c = new sku(sku_details_data);

        c.save(function () {
            res.send('record saved successfully');
        });

        // sku_detail_data.push(sku_details_data);
    }

    }


}

function dateCheck(from,to,check) {

    var fDate,lDate,cDate;
    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);

    if((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    return false;
}


var get_total_amount_group_by_disti = function(inv, done_callback) {
console.log('inside async');
    sales_registry.get_current_month_total_amount()
        .then (
        function (results) {
            console.log('result async');
            console.log(results);

            var out_results=[];

            try
            {
                if (results.length > 0) {


                    out_results.push(results);

                    return done_callback(null, out_results);

                }
                else{


                    out_results.push(results);
                    return done_callback(null, out_results);
                }
            }
            catch (ex)
            {

                console.log(ex);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}


// Get Active SKU's

var get_all_active_sku = function(inv, done_callback) {
   // console.log('inside async');
    sku.get_all_sku_master()
        .then (
        function (results) {
           // console.log('result async');
            console.log(results);

            var out_results=[];

            try
            {
                if (results.length > 0)
                {

                    out_results.push(results);

                    return done_callback(null, out_results);

                }
                else{


                    out_results.push(results);
                    return done_callback(null, out_results);
                }
            }
            catch (ex)
            {

                console.log(ex);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}



// End Get Active SKU's



var search_invoice = function(inv, done_callback) {

    sales_registry.get_all_invoice(inv.invoice_number,inv.retailer)
        .then (
        function (results) {

            var out_results=[];

            try
            {
                if (results.length > 0) {


                    out_results.push(results);

                    return done_callback(null, out_results);

                }
                else{


                    out_results.push(results);
                    return done_callback(null, out_results);
                }
            }
            catch (ex)
            {

                    console.log(ex);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}


exports.add_invoice = function (req, res) {
    var l_claim_id = util.get_program_name() + "-" + shortid.generate();
    var programs=util.get_programs_details();
    var prog_name = get_program_name(programs, req.body.invoice_date);

    var amt_arr=[];
    var amt_val={user_id: req.body.user_id,track_date: new Date(),invoice_amount: req.body.invoice_amount,verified_amount:0};
    amt_arr.push(amt_val);

    var inv_date_arr=[];
    var inv_date_val={user_id: req.body.user_id,  inv_date: req.body.invoice_date , track_date: new Date()};
    inv_date_arr.push(inv_date_val);

    var  test = [];


    var claim_data = {
          claim_id: l_claim_id
        , user_id: req.body.user_id
        , active : 1
        , invoice_date: req.body.invoice_date //todo accept date here from caller
        , invoice_amount: req.body.invoice_amount
        , invoice_number: req.body.invoice_number
        , retailer: req.body.retailer
        , status : "pending"
        , supporting_doc: req.body.supporting_doc
        , program_name :prog_name
        , region: req.body.region
        , claim_details : []
        , approval_comments:[]
        , amount_track:amt_arr
        , inv_date_track:inv_date_arr
        , company_name: req.body.company_name
        , created_date: new Date()
        , modified_by: ""
        , modified_date: ""
        , retailer_code:req.body.retailer_code
        , retailer_name:req.body.retailer_name
        , ispoint_calculated:1
        , verified_amount:0
        , participant_id: req.body.participant_id
        , upload_from:req.body.upload_from
        , name: req.body.name
        , distributor_code: req.body.distributor_code
        , state: req.body.state
    };

    test.push(claim_data);
    ///  var  check = search_invoice(req.body.invoice_number);


    async.map(test, search_invoice, function (err, r) {


                  if(r[0][0].length > 0)
                  {
                      res.send('Invoice Exist');

                  }
                  else
                  {
                        var c = new sales_registry(claim_data);

                         c.save(function () {
                         res.send('Claim saved,Please check status in MY CLAIM page');
                        });


                  }

          //   console.log(r[0][0].length);

      //  res.send(results);


    });


};


exports.update_invoice = function (req, res) {
    user.update_invoice(req.body.data)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_invoice_by_name  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.get_invoice_by_name (req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_invoice_details_by_name_inv_no  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.get_invoice_details_by_name_inv_no (req.params)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_invoice_details_by_name_by_inv  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.get_invoice_details_by_name_by_inv (req.body)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_sku_wise_detail_by_inv  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sku_wise_point_history.get_sku_wise_detail_by_inv (req.body)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};








exports.get_invoice_by_retailer  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.get_invoice_by_retailer (req.params.retailer)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.search_duplicate_invoice_list  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.search_duplicate_invoice_list (req.body.invoice_number)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.update_invoice_claim = function (req, res) {

    var programs=util.get_programs_details();
    var prog_name="";
    if(req.body.invoice_date!="Invalid Date"){
        prog_name = get_program_name(programs, req.body.invoice_date);
    }

    if(req.body.sku_master_array != 0)
    {
    save_part_code_details(req.body.sku_master_array);
    }
    sales_registry.update_invoice_claim(req.body ,prog_name)
        .then (
        function (results){
            res.send("claimms updated successfully");
            //send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.update_claim = function (req, res) {
    sales_registry.update_claim(req.body.data)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.get_invoice_by_number  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.get_invoice_by_number (req.params.claim_id)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.get_invoice_by_user_id  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);

    sales_registry.get_invoice_by_user_id (req.body.inv_date,req.body.inv_no,req.body.inv_amount)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_invoice_details_by_user_id  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sales_registry.get_invoice_details_by_user_id (req.params.user_id)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.get_early_bird_winners  = function (req, res) {

    sales_registry.get_early_bird_winners (req.params.program_type)
        .then (
        function (results){
            if (results.length > 0) {

                for(var i = results.length; i--;) {
//                    console.log(results[i].total_sale);
                    if(results[i].total_sale<100000) {
                        results.splice(i, 1);
                    }
                }
            }

            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_all_points_eligible_users  = function (req, res) {

    sales_registry.get_all_users()
        .then (
        function (results) {

            console.log('All Users ');
            console.log(results.length);
            if (results.length > 0) {

//                console.log ("Execute Time : " + Execute_Time + " Getting Verified & Approved Invoices User Info For Schneider.....");
//                console.log ("Execute Time : " + Execute_Time + " Total User : " + results.length);

                for (var i = 0; i < results.length; i++)
                {
                    results[i].points_credited = 0;
                    results[i].total_sale_nrml = 0;
                    results[i].total_sale_spcl = 0;
                    results[i].sum_of_total_sale_nrml_spcl = 0;
                    results[i].bonus_points_on_indpndsday = 0;
                    results[i].normal_points_on_total_sale = 0;
                    results[i].total_calc_points = 0;
                    results[i].current_points_balance = 0;
                    results[i].eligible_slab = 0;
                    results[i].req_point_next_slab = 0;
                }



                //Get Already Credited Points For These Users
                async.map(results, get_credited_points, function (err, r) {

                    for (var i = 0; i < r.length; i++) {
                        if(r[i][0].point_received==null){
                            r[i][0].point_received=0;
                        }
                        if(results[i]._id==r[i][0]._id)
                        {
                            results[i].points_credited = r[i][0].point_received;
                        }

                    }

                    //Get All User Wise Sales Information
                    async.map(results, get_all_eligible_user_wise_sales_data, function (err, r) {

                        for (var i = 0; i < r.length; i++) {

                            if(results[i]._id==r[i][0]._id)
                            {
                                results[i].total_sale_nrml = (r[i][0].total_sale_nrml * 1);
                                results[i].total_sale_spcl = (r[i][0].total_sale_spcl * 1);
                                results[i].sum_of_total_sale_nrml_spcl = (r[i][0].sum_of_total_sale_nrml_spcl * 1);
                                results[i].bonus_points_on_indpndsday = (r[i][0].bonus_points_on_indpndsday * 1);
                                results[i].normal_points_on_total_sale= (r[i][0].normal_points_on_total_sale * 1);
                                results[i].total_calc_points=((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale* 1));
                                results[i].current_points_balance=(((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale * 1))-(results[i].points_credited * 1));
                            }

                        }

//                          console.log('Result Set For All Users ');
//                          console.log(results);


                        //If Current Balance Is Less Than Equal To 0 Then Delete This User.
//                        for (var i = results.length; i--;) {
//                            if ((results[i].current_points_balance * 1) <= 0) {
//                                results.splice(i, 1);
//                            }
//                        }

                        for (var i = 0; i < results.length; i++) {

                            var p = [];
                            if (util.calculate_points(results[i].total_calc_points, "mixed").length == 0) {
                                p[0] = 0
                            }
                            else {
                                p = util.calculate_points(results[i].total_calc_points, "mixed");
                            }
                            results[i].eligible_slab = p[0];
                            results[i].req_point_next_slab=((results[i].eligible_slab * 1)- (results[i].points_credited * 1));

                        }

//                    //If He Is Not Eligible For Any Slab Then Delete This User.
//                        for (var i = results.length; i--;) {
//                            if ((results[i].eligible_slab * 1) <= 0) {
//                                results.splice(i, 1);
//                            }
//                        }
//
//                    //If Required Point For Next Slab Is Zero Then Delete This User.
//
//                        for (var i = results.length; i--;) {
//                            if ((results[i].req_point_next_slab * 1) <= 0) {
//                                results.splice(i, 1);
//                            }
//                        }

//                        console.log("Execute Time : " + Execute_Time + ' Result Set For Eligible Users For Schneider.');
//                        console.log(results);
                        console.log('Result Set For All Users ');
                        console.log(results.length);
                        console.log(results);

                        res.send(results);


                    });

                });

            }


        },
        function (err) {
            res.send(err);
        });


};

var get_credited_points = function(r, done_callback) {


    get_user_wise_credited_point(r._id,  function(results) {

        if (results.length > 0) {
            return done_callback(null, results);
        }
        else
        {
            var first_val= {_id : null, "point_received" : 0};
            results.push(first_val);
            return done_callback(null, results);
        }

    });

}

function get_user_wise_credited_point(user_id, callback) {

    var conn = mysql.get_mysql_connection();
    var sql = "select user_id as _id , sum(txn_amount) as point_received from user_points where " +
        "user_id='"+user_id+"' and program_type='Mixed' and txn_type='c'; ";


    conn.query(sql, function (error, results) {
        if (error) {

        }

        if (results.length  > 0) {
            callback(results);
        }

        conn.end();
        conn = null;

    });
}

var get_all_eligible_user_wise_sales_data = function(r, done_callback) {

    sales_registry.get_user_wise_total_claim(r._id)
        .then (
        function (results) {

            var out_results=[];

            if (results.length > 0) {

                var total_sale_nrml = 0;
                var total_sale_spcl = 0;
                var sum_of_total_sale_nrml_spcl=0;
                var total_point_on_nrml_spcl = 0;
                var sum_of_total_points_earned = 0;
                var bonus_points_on_indpndsday=0;
                var normal_points_on_total_sale=0;

                for (var i = 0; i < results.length; i++) {

                    if(results[i].program_name=="nrml")
                    {

                        total_sale_nrml=(results[i].total_sale * 1);

                    }
                    else  if(results[i].program_name=="spl1")
                    {
                        var p = [];

                        total_sale_spcl=(results[i].total_sale* 1);

                        if (util.calculate_points(results[i].total_sale, results[i].program_name).length == 0) {
                            p[0] = 0
                        }
                        else {
                            p[0] = util.calculate_points(results[i].total_sale, results[i].program_name);
                        }

                        bonus_points_on_indpndsday = p[0];

                    }

                }

                normal_points_on_total_sale=Math.round((total_sale_nrml + total_sale_spcl) / 1000);

                var first_val= {_id : results[0].user_id, "total_sale_nrml" : total_sale_nrml, "total_sale_spcl" : total_sale_spcl, "sum_of_total_sale_nrml_spcl" : (total_sale_nrml + total_sale_spcl), "bonus_points_on_indpndsday": bonus_points_on_indpndsday, "normal_points_on_total_sale": normal_points_on_total_sale};
                out_results.push(first_val);

                return done_callback(null, out_results);

            }
            else{

                var first_val= {_id : null, "total_sale_nrml" : 0, "total_sale_spcl" : 0, "sum_of_total_sale_nrml_spcl" : 0, "bonus_points_on_indpndsday" :0, "normal_points_on_total_sale": 0};
                out_results.push(first_val);
                return done_callback(null, out_results);
            }


        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

exports.get_points_credited_users  = function (req, res) {

    var conn = mysql.get_mysql_connection();
    var sql = "call get_users_points_credited ('schneider')";

    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;


            if(rows.length>0){

            var results=rows[0];

            for (var i = 0; i < results.length; i++)
            {
                results[i].points_credited = (results[i].PointsAlloted * 1);
                results[i].total_sale_nrml = 0;
                results[i].total_sale_spcl = 0;
                results[i].sum_of_total_sale_nrml_spcl = 0;
                results[i].bonus_points_on_indpndsday = 0;
                results[i].normal_points_on_total_sale = 0;
                results[i].total_calc_points = 0;
                results[i].current_points_balance = 0;
                results[i].cr_dr_points_balance = 0;
                results[i].eligible_slab = 0;
                results[i].req_point_next_slab = 0;
                results[i].away_next_slab = 0;
            }

                    //Get All User Wise Sales Information
            async.map(results, get_user_wise_sales_points, function (err, r) {

                        for (var i = 0; i < r.length; i++) {

                            if(results[i].email_id==r[i][0]._id)
                            {
                                results[i].total_sale_nrml = (r[i][0].total_sale_nrml * 1);
                                results[i].total_sale_spcl = (r[i][0].total_sale_spcl * 1);
                                results[i].sum_of_total_sale_nrml_spcl = (r[i][0].sum_of_total_sale_nrml_spcl * 1);
                                results[i].bonus_points_on_indpndsday = (r[i][0].bonus_points_on_indpndsday * 1);
                                results[i].normal_points_on_total_sale= (r[i][0].normal_points_on_total_sale * 1);
                                results[i].total_calc_points=((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale* 1));
                                results[i].current_points_balance=(((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale * 1))-(results[i].points_credited * 1));
                                results[i].cr_dr_points_balance=((results[i].current_points_balance*1)+(results[i].PointsBalance * 1));
                            }

                        }

                        //  console.log('Result Set For All Users ');
                        //  console.log(results);

                        for (var i = 0; i < results.length; i++) {

                            var p = [];
                            if (util.calculate_points(results[i].total_calc_points, "mixed").length == 0) {
                                p[0] = 0
                            }
                            else {
                                p = util.calculate_points(results[i].total_calc_points, "mixed");
                            }
                            results[i].eligible_slab = p[0];
                            results[i].req_point_next_slab=((results[i].eligible_slab * 1)- (results[i].points_credited * 1));

                            if(((results[i].cr_dr_points_balance*1)<4000) && ((results[i].cr_dr_points_balance*1)>=0))
                            {

                                results[i].away_next_slab= 500 - (results[i].cr_dr_points_balance % 500);

                            }
                            else
                            {

                                results[i].away_next_slab=0;
                            }

                        }

                        res.send(results);

            });

            }
        });
};

//Added By Hassan :Get User Wise Points Details Start

exports.get_user_wise_points_details  = function (req, res) {

    var conn = mysql.get_mysql_connection();
    var sql = "call get_users_points_credited ('blaupunkt')";

    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;

            if(rows.length>0) {

                var results = rows[0];

                for (var i = 0; i < results.length; i++)
                {
                    results[i].total_sku_pts = 0;
                    results[i].total_sale=0;
                    results[i].user_id=results[i].email_id;
                    results[i].req_point_next_slab = 0;
                    results[i].away_next_slab = 0;
                    results[i].earned_minus_redeem = 0;
                }

                async.map(results, get_user_wise_sku_points, function (err, r) {
                    _.each(results, function(element, index, list){
                        var sale_sku_point= _.where(r[0], {user_id: element.email_id});
                        if(sale_sku_point.length>0){

                            var current_total_point=parseInt(sale_sku_point[0].total_sku_pts);
                            var current_point_bal= parseInt(sale_sku_point[0].total_sku_pts)-parseInt(element.REDEEM_POINTS);

                            if(current_point_bal > 0 && current_point_bal <= 2000 ){
                                element.away_next_slab=2000-current_point_bal;
                            }
                            else if(current_point_bal > 2000 && current_point_bal <= 4000 ){
                                element.away_next_slab=4000-current_point_bal;
                            }
                            else if(current_point_bal > 4000 && current_point_bal <= 6000 ){
                                element.away_next_slab=6000-current_point_bal;
                            }
                            else if(current_point_bal > 6000 && current_point_bal <= 9000 ){
                                element.away_next_slab=9000-current_point_bal;
                            }
                            else if(current_point_bal > 9000 && current_point_bal <= 15000 ){
                                element.away_next_slab=15000-current_point_bal;
                            }
                            else if(current_point_bal > 15000 && current_point_bal <= 25000 ){
                                element.away_next_slab=25000-current_point_bal;
                            }
                            else if(current_point_bal > 25000 && current_point_bal <= 50000 ){
                                element.away_next_slab=50000-current_point_bal;
                            }
                            else if(current_point_bal > 50000){
                                element.away_next_slab=1000000-current_point_bal;
                            }

                            element.earned_minus_redeem=current_point_bal;
                            element.req_point_next_slab=parseInt(sale_sku_point[0].total_sku_pts)-parseInt(element.REDEEM_POINTS);

                            _.extend(element,sale_sku_point[0]);
                        }
                    });

                    console.log(results);
                    res.send(results);

                });

            }

        });
};


var get_user_wise_sku_points = function(r, done_callback) {

    sales_registry.get_user_wise_sku_points()
        .then (
        function (results) {
            try
            {
                if (results.length > 0) {
                    return done_callback(null, results);
                }
                else{
                    return done_callback(null, []);
                }
            }
            catch (ex)
            {
                //return done_callback(null, []);
                console.log(ex);
            }
        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

//Ended By Hassan



var get_user_wise_sales_points = function(r, done_callback) {

    sales_registry.get_user_wise_total_claim(r.email_id)
        .then (
        function (results) {

            var out_results=[];

                try
                {
                    if (results.length > 0) {
                    var total_sale_nrml = 0;
                    var total_sale_spcl = 0;
                    var sum_of_total_sale_nrml_spcl=0;
                    var total_point_on_nrml_spcl = 0;
                    var sum_of_total_points_earned = 0;
                    var bonus_points_on_indpndsday=0;
                    var normal_points_on_total_sale=0;

                    for (var i = 0; i < results.length; i++) {

                        if(results[i].program_name=="nrml")
                        {

                            total_sale_nrml=(results[i].total_sale * 1);

                        }
                        else
                        {
                            total_sale_spcl=(results[i].total_sale* 1);
                            var bonus = [];
                            if (util.calculate_points(results[i].total_sale, results[i].program_name).length == 0)
                            {
                                bonus[0] = 0
                            }
                            else {
                                bonus[0] = util.calculate_points(results[i].total_sale, results[i].program_name);
                            }

                            bonus_points_on_indpndsday = bonus[0];

                        }

                    }

                    normal_points_on_total_sale=Math.round((total_sale_nrml + total_sale_spcl) / 1000);

                    var first_val= {_id : results[0].user_id, "total_sale_nrml" : total_sale_nrml, "total_sale_spcl" : total_sale_spcl, "sum_of_total_sale_nrml_spcl" : (total_sale_nrml + total_sale_spcl), "bonus_points_on_indpndsday": bonus_points_on_indpndsday, "normal_points_on_total_sale": normal_points_on_total_sale};
                    out_results.push(first_val);

                    return done_callback(null, out_results);

                }
            else{

                    var first_val= {_id : null, "total_sale_nrml" : 0, "total_sale_spcl" : 0, "sum_of_total_sale_nrml_spcl" : 0, "bonus_points_on_indpndsday" :0, "normal_points_on_total_sale": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
                }
                catch (ex)
                {

//                    console.log(ex);
                }




        },
        function (err) {
            //            res.send(err);
            return done_callback(null, err);
        }
    );

}

exports.get_my_order_details = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_user_wise_order ('" + req.params.user_id + "','"+0+"')";


    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

}


exports.get_all_distributor = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_distributor ('" + req.params.p_id + "')";

    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

}

exports.get_shipping_detail_by_order_id = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call GetShippingDetail (" + req.params.order_id +")";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

}



exports.get_all_schneider_users = function(req, res) {
    var conn = mysql.get_mysql_connection();


    var sql = "call get_schneider_user_data_pagewise ('" + req.params.company + "',0,'" + req.params.page_no + "',200,0,0)";

    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

}

exports.get_order_information = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call get_order_information (" + req.params.order_id +")";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

}


//Schneider Report

exports.get_top_si = function (req, res) {
    sales_registry.get_top_si(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_top_dealer_by_points = function (req, res) {
    sales_registry.get_top_dealer_by_points()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_dealer_sku_qty_wise = function (req, res) {
    sales_registry.get_dealer_sku_qty_wise()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.get_top_si_by_region = function (req, res) {
    sales_registry.get_top_si_by_region(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_si_by_region = function (req, res) {
    sales_registry.get_si_by_region(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.special_program_qualifier = function (req, res) {
    sales_registry.special_program_qualifier(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.normal_program_qualifier = function (req, res) {
    sales_registry.normal_program_qualifier(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};




exports.get_claim_by_user = function (req, res) {
    sales_registry.get_claim_by_user(req.params.user)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_invoice_by_status_comp_name = function (req, res) {
    sales_registry.get_invoice_by_status_comp_name(req.params.status,req.params.company_name)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};





exports.search_by_sku  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sku.search_by_sku (req.params.sku)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.search_availability_by_sku  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sku_tran.search_availability_by_sku (req.params.sku)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_sku_by_name  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sku.get_sku_by_name (req.params.name)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.search_by_part_code  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sku.search_by_part_code (req.body.part_code)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.search_sku_list  = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    sku.search_sku_list (req.params.sku)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};






//***************************//




//********* program **********methods





exports.get_region_by_level= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    region.get_region_by_level (req.params.level1,req.params.level2)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};
exports.get_all_region= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    region.get_all_region ()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_region_by_id= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    region.get_region_by_id (req.params.id)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_specific_region_by_id= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    region.get_specific_region_by_id (req.params.id)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }
    );
};



exports.save_region = function (req, res) {
    var region_data = {
          company:req.body.company
        , id:req.body.id
        , name: req.body.name
        , parent_id:req.body.parent_id
        , description: req.body.description
        , level: req.body.level
    };
    var p1 = new region(region_data);
    p1.save(function (req) {
        res.send('item saved');
    });

};


// ********* program ****************
//arnab added for programe/add/
exports.add_program = function (req, res) {
    var program_data = {
          company : req.body.company
        , id: req.body.id
        , name: req.body.name
        , description: req.body.description
        , region: req.body.region
        , start_date:new Date( req.body.start_date)
        , end_date:new Date( req.body.end_date)
    };
    var p = new program(program_data);
    p.save(function (req) {
        res.send('Program Saved Successfully');
    });

};

exports.get_all_program = function (req, res) {
    program.get_all_program()
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }
    );
};

exports.get_program_by_id= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    program.get_program_by_id (req.params.id)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};
exports.get_program_by_company_region= function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    program.get_program_by_company_region( req.params.company, req.params.region, new Date())
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

// ********* end program ****************


//********************  user  methods   *************************************
 exports.user_login = function(req, res) {



    var conn = mysql.get_mysql_connection();

   // var sql = "call user_login_new ('" + req.body.email_id + "','" + req.body.password  + "','" + req.body.company + "')";

     var sql = "call system_user_login('" + req.body.email_id + "','" + req.body.password  + "','" + req.body.company + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            if(rows[0][0].msg == 'login successful') {

                rows[0][0].token = token.assign({email_id: req.body.email_id});
            }
            res.send(rows);
        });

}

exports.terms_condition_update = function(req, res) {

    var conn = mysql.get_mysql_connection();



    var sql = "call update_terms_cond_info ('" + req.body.email_id + "','" + req.body.terms_status  + "','"+req.body.terms_date+"')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info("user terms and condition validated from mysql");
            conn.end();
            conn = null;

            //rows[0][0].token = token.assign({email_id: req.body.email_id});
            res.send('T&C Updated');
        });

}



exports.get_user_company_name = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select distinct company_name from xolo_customer_info a, user_registration b " +
               "where a.email_id=b.email_id and b.company='" + req.params.company + "' and company_name like '" + req.params.srch_key + "%';";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
        });

}


exports.change_password = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call change_password  ('"    + req.body.email_id + "','"
                                            + req.body.company + "', '"
                                            + req.body.old_password  + "','"
                                            + req.body.new_password  + "','"
                                             +''+ "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info("password change");
            conn.end();
            conn = null;
            res.send(rows);
        });

}


//mysql company get method

exports.get_company = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT id,name FROM company;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info(" validate from mysql");
            res.send(rows);
        });
};



exports.get_user_details = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT first_name,last_name,email_id FROM user_registration where company='"+req.params.company+"' order by first_name;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info(" validate from mysql");
            res.send(rows);
        });
};


exports.save_user = function (req, res) {
    var user_data = {
        user_id:req.body.user_id
        , company:req.body.company
        , region_id: req.body.region_id
        , region_name: req.body.region_name
        , title: req.body.title
    };
    var p1 = new user(user_data);
    p1.save(function (req) {
        res.send('item saved');
    });

};


exports.update_user_basic_info = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var dobFormat= new Date(req.body.user_dob).toFormat('YYYY-MM-DD');
//    var dojFormat= new Date(req.body.comp_found_date).toFormat('YYYY-MM-DD');

    var dobFormat= req.body.user_dob;
    var dojFormat= req.body.comp_found_date;


//    var sql = "update user_registration set first_name='"+req.body.first_name+"', " +
//        "last_name='"+req.body.last_name+"'," +
//        "mobile_no='"+req.body.mobile_no+"'," +
//        "dob='"+ dobFormat +"'," +
//        "doj='"+ dojFormat +"'," +
//        "gender='"+ req.body.gender+"'," +
//        "qualification='"+req.body.qualification+"'," +
//        "user_pic='"+req.body.profile_img+"' where email_id='"+req.body.email_id+"';";

    var sql = "call update_user_details('"+'user_pic'+"','"
        +req.body.first_name+ "', '"
        +req.body.last_name+ "','"
        +req.body.mobile_no+ "','"
        + dobFormat + "','"
        + dojFormat + "','"
        + req.body.gender+ "','"
        +req.body.qualification+ "','"
        +req.body.profile_img+ "','"
        +req.body.email_id+ "')";

    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info("update user info");
            conn.end();
            conn = null;
            res.send('update user information successfully');
        });

}

exports.update_user_address = function(req, res) {
    var conn = mysql.get_mysql_connection();


    var sql = "call update_user_info  ('"    + req.body.first_name + "','"
        + req.body.last_name + "', '"
        + req.body.gender  + "','"
        + req.body.email_id  + "','"
        + req.body.mobile_no  + "','"
        + req.body.office_no  + "','"
        + req.body.street  + "','"
        + req.body.city  + "','"
        + req.body.address  + "','"
        + req.body.state  + "','"
        + req.body.country  + "','"
        + req.body.zipcode  + "','"
        + ""  + "','"
        + 2  + "','"
        + new Date()  + "','"
        + req.body.company + "','"
        + "" + "','"
        + new Date()  + "','"
        + 1  + "','"
        + req.body.company_address + "','"
        + req.body.reward_delivery_address + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info("update address");
            conn.end();
            conn = null;
            res.send('update user address successfully');
        });

}






//******************** end  post *************************************


//*********************PROFILE START*****************************//
exports.get_user_info= function(req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call user_profile_summary ('" + req.params.email_id + "')";
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            logger.info(" validate from mysql");
            res.send(rows);
        });

};

//*************************PROFILE END************************//

//*************************AWS START************************//

exports.get_upload_url= function(req, res) {
    aws.config.loadFromPath('config/aws.config.json')
//    var s3 = new aws.S3({computeChecksums: true});
    var s3 = new aws.S3();
    var params = {Bucket: util.get_aws().bucket,
          Key: req.params.filename, Expires: util.get_aws().upload_link_expiry};
    var url = s3.getSignedUrl('putObject', params);
    res.send (url);
};
var upload_file = function(){
    var filename;
    var filesize;
    var filetype;
}
//** Function to save files to disk
function onFileSaveToDisk(fieldname, file, filename, encoding,  res) {

    var done;
    var fstream = fs.createWriteStream(path.join('./upload', path.basename(filename)));
    file.once('end', function() {
        console.log(fieldname + '(' + filename + ') EOF');
    });
    fstream.once('close', function() {
        console.log(fieldname + '(' + filename + ') written to disk');
//        var stream = fs.createReadStream('upload/' + filename)
        var s3client = knox.createClient({
            key: util.get_aws().AWS_KEY,
            secret: util.get_aws().AWS_SECRET,
            bucket: util.get_aws().bucket
        });
        var header =  {'x-amz-acl': 'public-read'};

        var f = 'upload/' + filename;
        s3client.putFile(f, filename,{ 'x-amz-acl': 'public-read' },
            function(err, r) {
                if (err) {
                    console.log("Error PUTing file in S3:", err);
                }
//                console.log(r);
                if (r.statusCode == 200){
                    console.log(r.req.url);
                    console.log("S3 RESPONSE:", r.statusCode);
                    // ready to delete
                    fs.unlink(f, function (err) {
                        if (err) throw err;
                        console.log('successfully deleted ' + f);
                        res.send (r.req.url);
                        //return true;//res.send( r.url);
                    });
             }

            });
        //next();
    });
    console.log(fieldname + '(' + filename + ') start saving');
    file.pipe(fstream);
}


exports.upload_file= function(req, res) {
    busboy = new Busboy({ headers: req.headers });
    var infiles, outfiles, done;

    busboy.on('file', function(fieldname, file, filename, encoding) {
       // ++infiles;
        filename = "blaupunkt-" + shortid.generate() + "." + filename.substring(filename.length-3, filename.length);
        filename = filename.replace(/\s+/g, '');

        onFileSaveToDisk(fieldname, file, filename, encoding, res, function() {
//        onFileSaveToS3(fieldname, file, filename, encoding, function() {
            //++outfiles;
            if (done){
                // ACTUAL EXIT CONDITION
                console.log('All parts written to disk');
//                request.reply({ msg: filename}).type('text/html');

            }
        });
    });

    busboy.once('end', function() {
        console.log('Done parsing form!');
        done = true;
    });
    req.pipe(busboy);
};

//exports.upload_file= function(req, res) {
//    var s3Client = knox.createClient({
//        key: util.get_aws().AWS_KEY,
//        secret: util.get_aws().AWS_SECRET,
//        bucket: util.get_aws().bucket
//    });
//    var object = { testing: "1234567890" };
//    var string = JSON.stringify(object);
//    var req = s3Client.put('/test/obj1.json', {
//        'Content-Length': string.length,
//        'Content-Type': 'application/json'
//    });
//    req.on('response', function (res) {
//        if (200 == res.statusCode) {
//            console.log('saved to %s', req.url);
//            //res.send(req.url);
//        }
//        else {
//            console.log(req);
//
//            //res.send ("error");
//        }
//    });
//
//}
//exports.get_upload_url= function(req, res) {
//    var s3Client = knox.createClient({
//        key: util.get_aws().AWS_KEY,
//        secret: util.get_aws().AWS_SECRET,
//        bucket: util.get_aws().bucket
//    });
//    var expires = new Date();
//    expires.setMinutes(expires.getMinutes() + 10);
//    var url= s3Client.signedUrl(req.params.filename, expires);
//    res.send (url);
//};

//*************************AWS END************************//

// *************************** Counter Number ****************//

exports.get_counter_no = function (req, res) {
    counter.get_counter_no()
        .then (
        function (results) {
            if (results.length > 0) {
                send_to_response(results, res);
            }
            else {
                send_empty_recordset(res);
            }
        },
        function (err) {
            res.send(err);
        }

    );
};

exports.update_counter_number = function (req, res) {
    counter.update_counter_no(req.body.id)
        .then (
        function (results) {
        send_to_response({"msg":"updated"}, res);
        },
        function (err) {
            res.send(err);
        }
    );
};

// ************************************************************//



// ******************* private helper functions ***********************
var send_to_response = function(results, res ){

    res.contentType('application/json');
    if (util.is_array(results)){
        var arr = [];
        results.forEach(function(r){
            arr.push(r)
        });
        res.send(arr);
    }else {

        res.send(results);
    }
}



var return_back  = function(results ){
    var arr = [];
    results.forEach(function(claim){
        arr.push(claim)
    });
    return arr;
}
var send_empty_recordset = function( res ){
    var response = {"message": "no result found", "length": 0};

    res.contentType('application/json');
    res.send(response);
}

// ***********************************************************************
//limit and skip functionality
//Below functions are to be used by every function to ensure
//limit and skip values are legal
//and no error is generated as a result.
//Every developer needs to use these if they are implementing
// ***********************************************************************
var sanitize_limit = function (req) {
    var limit = req.params.limit;
    if ((limit === undefined) || (limit < 0 )) {
        limit = util.get_fetch_limit();
    }
    return limit;
}
var sanitize_skip = function (req) {
    var skip = req.params.skip;
    if ((skip === undefined) || (skip < 0 )) {
        skip = 0;
    }
    return skip;
}
// ***********************************************************************


 // ***************************Dashboard******************************************

exports.get_total_sale_branch = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select participant_id from program_commerce.participants_relation where parent_2_id = '"+ req.params.parent_id +"';"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.params.parent_id + " as params");
            logger.info(" validate from mysql");

            if(req.params.parent_id == 'DIST150405214438216')
                    {
                        if(rows.length > 0){
                            rows.push({participant_id: 201504041})
                        }
                    }
            console.log(rows);
            res.send(rows);
        });
};


exports.post_real_total_sale_branch = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    sku_wise_point_history.post_real_total_sale_branch(req.body)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};



exports.post_real_total_sale_branch_hq = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    sku_wise_point_history.post_real_total_sale_branch_hq(req.body)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_total_sale_branch_hq = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select participant_id from program_commerce.participants_relation where parent_3_id = '"+ req.params.parent_id +"';"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.params.parent_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_total_sale_dealer_normal = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select participant_id from program_commerce.participants_relation where parent_4_id = '"+ req.params.parent_id +"' and parent_4_type = '4';"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.params.parent_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.post_real_total_sale_dealer_normal = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    sku_wise_point_history.post_real_total_sale_dealer_normal(req.body)
        .then (
        function (results){
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_sku_of = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    sku_wise_point_history.get_sku_of(req.body)
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_pts_of = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    //sales_registry.get_pts_of(req.body)
    sku_wise_point_history.get_pts_of(req.body)
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_ptsdetails = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    //sales_registry.get_ptsdetails(req.params.participantid)
    sku_wise_point_history.get_ptsdetails(req.params.participantid)
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_saledetails = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    //sales_registry.get_saledetails(req.params.participantid)
    sku_wise_point_history.get_saledetails(req.params.participantid)
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_total_sale_dist = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select _id from program_commerce.hierarchy where parent_id = '"+ req.params.parent_id +"' and type_id = 20;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.params.parent_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.post_real_total_sale_dist = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var id = req.params.overall;
    var ids = id.replace(/["']/g, "");

    var sql = "select participant_id,participant_name, participant_code from program_commerce.participants where participant_id in ("+ ids +");"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.body + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.post_real_total_sale_dist_ser = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var id = req.params.p_id;
//    var ids = id.replace(/["']/g, "");

    var sql = "select participant_id from program_commerce.participants_relation where parent_2_id in ("+ req.params.p_id +");"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  console.log("Trying: "+ p_id + " as params");
            logger.info("validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};

// Edited by Dipanjan on 02/09/2015

exports.post_real_total_sale_dist_performer_aso = function (req, res) {

    var ids = [];
    var id ={i:0};
    ids.push(id);

    sku_wise_point_history.post_real_total_sale_dist_performer_aso(req.body)
        .then (
        function (results){



            async.map(ids, get_total_amount_group_by_disti, function (err, r) {



                var total_data = r[0][0];

                if(total_data.length > 0){
                    _.each(results, function(element, index, list){
                        var disti_sku_data=_.where(total_data, {retailer: element.distributor});
                        console.log('inside async details');
 //                       console.log(disti_sku_data);
//                        console.log(disti_sku_data[0].total_sale);
                        if(disti_sku_data.length > 0)
                        {
                            console.log('inside async1 details');
                            element.current_month_sale = disti_sku_data[0].total_sale;
                        }
                        else
                        {
                            element.current_month_sale = 0;
                        }
                        console.log(element);
                        //_.extend(element.current_month_sale, disti_sku_data.total_sale);
                    });
                }


                console.log('final data here details');
                console.log(results);
                send_to_response(results, res);

            })

        },
        function (err){
            res.send(err);
        }

    );
};

// End

exports.get_dets_nutshell = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    //sales_registry.get_dets_nutshell(req.body)
    sku_wise_point_history.get_dets_nutshell(req.body)
        .then (
        function (results){
            console.log("Hcddadada: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_participant_details = function (req, res) {
    var ids = [];
    var all_sku_data = [];
    var id = {};
    ids.push(id);
    sku_wise_point_history.get_participant_details(req.params.participantid)
        .then (
        function (results){

          //  console.log("Hi Dashboard!!!! : "+results);

            async.map(ids,get_all_active_sku, function (err, r) {
          //   console.log("SKU Master");
          //   console.log(r);
             var sku_list = r[0][0];
                if(results.length > 0){
                    _.each(sku_list, function(element, index, list){
                        var sku_data=_.where(results, {sku: element.sku});
                        // console.log('inside async details');
                        //                       console.log(disti_sku_data);
//                        console.log(disti_sku_data[0].total_sale);
                        if(sku_data.length > 0)
                        {
                           // console.log(sku_data);
                          //  console.log("inside if");
                            all_sku_data.push(sku_data[0]);
                        }

                     //   console.log("Last DataLast Data");
                     //   console.log(all_sku_data);
                        //_.extend(element.current_month_sale, disti_sku_data.total_sale);
                    });
                }

                send_to_response(all_sku_data, res);
            });

        },
        function (err){
            res.send(err);
        }

    );
};


// ALL RSM Functions

exports.get_people_under_rsm = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var id = req.params.p_id;
//    var ids = id.replace(/["']/g, "");

    var sql = "select _id ,name from program_commerce.hierarchy where parent_id = "+ req.params.p_id +" and type_id = 14;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  console.log("Trying: "+ p_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};

// Get top 5 ASO List

exports.get_all_aso = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var id = req.params.p_id;
//    var ids = id.replace(/["']/g, "");

    var sql = "select _id ,name from program_commerce.hierarchy where type_id = 14;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  console.log("Trying: "+ p_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};





// End

exports.get_people_under_ho = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var id = req.params.p_id;
//    var ids = id.replace(/["']/g, "");

    var sql = "select _id ,name from program_commerce.hierarchy where parent_id = "+ req.params.p_id +" and type_id = 12;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  console.log("Trying: "+ p_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_dist_aso_under_rsm = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var id = req.params.p_id;
    var ids = id.replace(/["']/g, "");

    var sql = "select _id from program_commerce.hierarchy where parent_id in ("+ ids +") and type_id = 20;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.body + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};

exports.get_disti_aso_under_rsm_ho = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var id = req.params.p_id;
    var ids = id.replace(/["']/g, "");

    var sql = "select _id from program_commerce.hierarchy where parent_id in ("+ ids +") and type_id = 14;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.body + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_distcode_under_rsm = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var id = req.params.p_id;
    var ids = id.replace(/["']/g, "");

    var sql = "select participant_id, participant_code from program_commerce.participants where participant_id in ("+ ids +");"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.body + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_disti_code_under_rsm = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var id = req.params.p_id;
    var ids = id.replace(/["']/g, "");

    var sql = "select participant_id, participant_code from program_commerce.participants where participant_id in ("+ ids +");"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.body + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_actual_participant_under_rsm = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var id = req.params.p_id;
//    var ids = id.replace(/["']/g, "");

    var sql = "select participant_id from program_commerce.participants_relation where parent_2_id in ("+ req.params.p_id +");"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  console.log("Trying: "+ p_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_participant_under_rsm_ho = function(req, res) {
    var conn = mysql.get_mysql_connection();
//    var id = req.params.p_id;
//    var ids = id.replace(/["']/g, "");

    var sql = "select participant_id from program_commerce.participants_relation where parent_2_id in ("+ req.params.p_id +");"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  console.log("Trying: "+ p_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};



exports.get_dets_nutshell_aso_sales = function (req, res) {

    var ids = [];
    var id ={i:0};
    ids.push(id);

    sku_wise_point_history.get_dets_nutshell_aso_sales(req.body)
        .then (
        function (results){

            async.map(ids, get_total_amount_group_by_disti, function (err, r) {

                var total_data = r[0][0];

                if(total_data.length > 0){
                    _.each(results, function(element, index, list){
                        var disti_sku_data=_.where(total_data, {retailer: element.distributor});
                       // console.log('inside async details');
                        //                       console.log(disti_sku_data);
//                        console.log(disti_sku_data[0].total_sale);
                        if(disti_sku_data.length > 0)
                        {
                            //console.log('inside async1 details');
                            element.current_month_sale = disti_sku_data[0].total_sale;
                        }
                        else
                        {
                            element.current_month_sale = 0;
                        }
                        console.log(element);
                        //_.extend(element.current_month_sale, disti_sku_data.total_sale);
                    });
                }


                console.log('final data here details');
                console.log(results);
                send_to_response(results, res);

            })
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_points_details = function (req, res) {
    //console.log("Hello");
   // console.log(req.body);
    sku_wise_point_history.get_points_details(req.body)
        .then (
        function (results){
            console.log("ASOSales: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_participant_rsmdetails = function (req, res) {
    sku_wise_point_history.get_participant_rsmdetails(req.params.participantid)
        .then (
        function (results){
          //  console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_participant_rsmdetails = function (req, res) {
    sku_wise_point_history.get_participant_rsmdetails(req.params.participantid)
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_real_aso_name = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select name from program_commerce.hierarchy where _id = '"+ req.params.participantid +"' and type_id = 14;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.params.parent_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};


exports.get_sku_of_all_rsm = function (req, res) {
    console.log("Hello");
    console.log(req.body);
    sku_wise_point_history.get_sku_of_all_rsm(req.body)
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};


exports.get_proc_call = function(req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_user_mapping()";


    console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

};
exports.get_sku_of_ho = function (req, res) {
    sku_wise_point_history.get_sku_of_ho()
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_total_sale_dist_ho_rsm_aso = function(req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT * from program_commerce.hierarchy where parent_id in (select _id FROM program_commerce.hierarchy where parent_id= '"+ req.params.parent_id +"' and type_id=14) and type_id=20;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err)
                res.end;
            }
            console.log("Trying: "+ req.params.parent_id + " as params");
            logger.info(" validate from mysql");
            console.log(rows);
            res.send(rows);
        });
};



exports.get_sales_bu = function (req, res) {
    sales_registry.get_sales_bu()
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_sales_bu_monthtwo = function (req, res) {
    sales_registry.get_sales_bu_monthtwo()
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

exports.get_sales_bu_monththree = function (req, res) {
    sales_registry.get_sales_bu_monththree()
        .then (
        function (results){
            console.log("Hi: "+results);
            send_to_response(results, res);
        },
        function (err){
            res.send(err);
        }

    );
};

// ********************End ***************************


this.renew_token = function(req, res, next){
    var token_data = token.verify(req.headers.token);
    var token_renw="";
    if(token_data) {
        token_renw = token.assign(token_data);
    }
    util.send_to_response(token_renw, res);

}


// ******************* private helper functions ***********************




