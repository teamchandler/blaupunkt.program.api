//<editor-fold desc="point to cover up">
//verification/status/detail --> verification_code / user_id --> bool true/false - post --> gives the validity
//verification/status/update --> verification_code / user_id --> bool true/false - post --> changes status
//</editor-fold>

//<editor-fold desc="Variable Declaration">

var user  = require('../repo/master/user_security')
var token = require('../token');
var util  = require('../util');
var crypto = require('crypto');

"use strict";
//</editor-fold>


this.validate_user = function(req, res, next){

    var retData = {};
    retData.msg= 'Log in incorrect';
    retData.token = "";
    var   query = {"user_id": req.body.user_id};
    user.get_list(query).then(function (result) {
            if(result.length>0 && result[0].pwd==req.body.pwd){
                retData.msg= 'Success';
                retData.token = token.assign({user_id: req.body.user_id});
                util.send_to_response(retData, res);

            }else{
                retData.msg='Log in incorrect';
                util.send_to_response(retData, res);
            }

        },
        function (err) {
            logger.info("error in login,err:" + err.message);
            retData.msg= err.message;
            util.send_to_response(retData, res);
        });

    }

this.renew_token = function(req, res, next){
    var token_data = token.verify(req.headers.token);
    var token_renw="";
    if(token_data) {
        token_renw = token.assign(token_data);
    }
    util.send_to_response(token_renw, res);

}

