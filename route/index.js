
//<editor-fold desc="Variable Declaration">

var error_handler = require('./error').errorHandler,
    master_handler = require('./master'),
    express =       require('express'),
    util =          require('../util.js'),
    repo        = require('../repo')
    ;


var logger =        util.get_logger("server");
// define routers (new in Express 4.0)
var ping_route          =   express.Router(),
    master_route        =   express.Router(),
    transaction_route   =   express.Router(),
    help_route          =   express.Router(),
    login_route         =   express.Router()
    ;

var token = require('../token');
var login = require('./login');


//</editor-fold>



module.exports = exports = function(app, corsOptions) {
    //<editor-fold desc="Fast Phase">
    // protect any route starting with "master" with validation
    app.all("/master/*", authenticate, function(req, res, next) {
        next(); // if the middleware allowed us to get here,
        // just move on to the next route handler
    });

    // implement ping_route actions
    // This is specific way to inject code when this route is called
    // This executes for any ping route.
    // Similarly we can (and should) implement same for every route
    ping_route.use (function(req,res,next){
        logger.info(util.create_routing_log(req.method, req.url, "ping", "PING"));
        // continue doing what we were doing and go to the route
        next();
    });
    ping_route.get('/', function(req, res){
        res.send({'error' : 0, 'msg' : 'audit endpoints ready for data capture'});
    });
    ping_route.get('/check/mongo', function(req, res){
        res.send({'error' : 0, 'msg' : 'Code for checking Mongo connection will be implemented here'});
    });
    ping_route.post('/check/post', function(req, res){
        res.send('Checking Post Method' + req.body);
    });
    app.use('/ping', ping_route);
    // end ping route

    var multipart = require('connect-multiparty');
    var multipartMiddleware = multipart();
    // master route implementation
    master_route.use (function(req,res,next){
        logger.info(util.create_routing_log(req.method, req.url, "master", "MASTER"));
        // continue doing what we were doing and go to the route
        next();
    });
    //</editor-fold>

    //<editor-fold desc="Add Code for new Collection">

    var user_master = master_handler.user_master;
//    var user_security_master = master_handler.user_security_master;
//    var proj_master = master_handler.proj_master;
//    var req_master = master_handler.req_master;
//    var testcase = master_handler.testcase_master;
//    var testscenario = master_handler.testscenario_master;
//    var testexecution = master_handler.testexecution_master;
    var api_master = master_handler.api_master;
    //<editor-fold desc="renew token 20141112 srijan">
    master_route.post('/renew/token', function(req, res){
        api_master.renew_token(req,res);
    });
    //</editor-fold>

    //<editor-fold desc="user 20141106 srijan">

    help_route.get('/user', function(req, res){
    user_master.help(req,res);
    });

    // Add Invoice

    master_route.post('/invoice/schneider/add', function(req, res){
        api_master.add_invoice(req,res);
    });

    master_route.get('c', function(req, res){
        api_master.get_retailer_list(req,res);
    });


    ////


    //*********************** Program Management**********************//

    master_route.get('/user/company', function(req, res){
        api_master.get_company(req,res);
    });
    master_route.get('/user/details/:company', function(req, res){
        api_master.get_user_details(req,res);
    });
    master_route.get('/userinfo/by/user/:email_id', function(req, res){
        api_master.get_user_info(req,res);
    });
    master_route.get('/order/detail/by/email/:user_id', function(req, res){
        api_master.get_my_order_details(req,res);
    });
    master_route.get('/get/shipping_details/by/order_id/:order_id', function(req, res){
        api_master.get_shipping_detail_by_order_id(req,res);
    });
    master_route.get('/get/order_information/by/order_id/:order_id', function(req, res){
        api_master.get_order_information(req,res);
    });
    master_route.get('/program/list/all', function(req, res){
        api_master.get_all_program(req,res);
    });
    master_route.get('/program/by/id/:id', function(req, res){
        api_master.get_program_by_id(req,res);
    });
    master_route.get('/program/list/company_region/:company/:region', function(req, res){
        api_master.get_program_by_company_region(req,res);
    });

    master_route.post('/user/basic/info/update', function(req, res){
        api_master.update_user_basic_info(req,res);
    });

    master_route.post('/user/basic/address/update', function(req, res){
        api_master.update_user_address(req,res);
    });
    master_route.post('/program/save', function(req, res){
        api_master.add_program(req,res);
    });

  //  app.get('/store/my_order_details/email/:user_id',user.get_my_order_details);

    //****//


// ********* Retailer Services ********************

    master_route.get('/retailer/list/:p_id', function(req, res){
        api_master.get_all_distributor(req,res);
    });

    // ********* Workflow Services ********************



    // **************** Dashboard Distributor****************************

    master_route.get('/program/branch/total/sale/:parent_id', function(req, res){
        api_master.get_total_sale_branch(req,res);
    });
    master_route.post('/program/real/branch/total/sale', function(req, res){
        api_master.post_real_total_sale_branch(req,res);
    });

    master_route.post('/program/sku/toper', function(req, res){
        api_master.get_sku_of(req,res);
    });
    master_route.post('/program/pts/acc', function(req, res){
        api_master.get_pts_of(req,res);
    });

    master_route.post('/program/sku/top/ho', function(req, res){
        api_master.get_sku_of_ho(req,res);
    });

    // *********************** End Dashboard Distributor ****************


    // **************** Dashboard Dealer HQ****************************

    master_route.get('/program/branch/total/sale/hq/:parent_id', function(req, res){
        api_master.get_total_sale_branch_hq(req,res);
    });

    master_route.post('/program/real/branch/total/sale/hq', function(req, res){
        api_master.post_real_total_sale_branch_hq(req,res);
    });


    // *********************** End Dashboard Dealer HQ****************

    // **************** Dashboard Dealer Normal****************************

    master_route.get('/program/dealer/normal/sale/:parent_id', function(req, res){
        api_master.get_total_sale_dealer_normal(req,res);
    });
    master_route.post('/program/real/dealer/normal/sale', function(req, res){
        api_master.post_real_total_sale_dealer_normal(req,res);
    });

    master_route.get('/program/pts/details/:participantid', function(req, res){
        api_master.get_ptsdetails(req,res);
    });

    master_route.get('/program/get/sale/amount/details/:participantid', function(req, res){
        api_master.get_saledetails(req,res);
    });
    
     master_route.get('/program/dist/total/sale/:parent_id', function(req, res){
        api_master.get_total_sale_dist(req,res);
    });
    master_route.get('/program/real/dist/total/sale/:overall', function(req, res){
        api_master.post_real_total_sale_dist(req,res);
    });

    master_route.get('/program/real/dist/total/sale/ser/:p_id', function(req, res){
        api_master.post_real_total_sale_dist_ser(req,res);
    });

    master_route.post('/program/real/dist/total/sale/performer/aso', function(req, res){
        api_master.post_real_total_sale_dist_performer_aso(req,res);
    });
    master_route.post('/program/details/pager/aso/sales', function(req, res){
        api_master.get_dets_nutshell_aso_sales(req,res);
    });

    //RSM
    master_route.get('/program/get/people/under/rsm/:p_id', function(req, res){
        api_master.get_people_under_rsm(req,res);
    });
    master_route.get('/program/get/people/under/ho/:p_id', function(req, res){
        api_master.get_people_under_ho(req,res);
    });
    master_route.get('/program/get/dist/aso/under/rsm/:p_id', function(req, res){
        api_master.get_dist_aso_under_rsm(req,res);
    });

    master_route.get('/program/get/disti/aso/under/rsm/ho/:p_id', function(req, res){
        api_master.get_disti_aso_under_rsm_ho(req,res);
    });


    master_route.get('/program/get/distcode/under/rsm/:p_id', function(req, res){
        api_master.get_distcode_under_rsm(req,res);
    });
    master_route.get('/program/get/disti_code/under/rsm/:p_id', function(req, res){
        api_master.get_disti_code_under_rsm(req,res);
    });
    master_route.get('/program/get/actual/participant/under/rsm/:p_id', function(req, res){
        api_master.get_actual_participant_under_rsm(req,res);
    });

    master_route.get('/program/get/participants/under/rsm/ho/:p_id', function(req, res){
        api_master.get_participant_under_rsm_ho(req,res);
    });
    master_route.get('/program/parti/rsm/details/:participantid', function(req, res){
        api_master.get_participant_rsmdetails(req,res);
    });
    master_route.get('/program/get/aso/name/:participantid', function(req, res){
        api_master.get_real_aso_name(req,res);
    });
    master_route.post('/program/sku/all/rsm', function(req, res){
        api_master.get_sku_of_all_rsm(req,res);
    });
    master_route.get('/program/get/rsm/proc/', function(req, res){
        api_master.get_proc_call(req,res);
    });

    //Added By Hassan Ahamed
    master_route.get('/disti/wise/sales/data', function(req, res){
        api_master.get_disti_wise_sales_data(req,res);
    });

//    login_route.get('/disti/wise/sales/data', function(req, res){
//        api_master.get_disti_wise_sales_data(req,res);
//    });

    master_route.get('/disti/wise/sku/sales/data', function(req, res){
        api_master.get_disti_wise_sku_sales_data(req,res);
    });

    master_route.get('/program/dist/total/sale/ho/rsm/aso/:parent_id', function(req, res){
        api_master.get_total_sale_dist_ho_rsm_aso(req,res);
    });

    master_route.get('/program/sales/break/up/', function(req, res){
        api_master.get_sales_bu(req,res);
    });

    master_route.get('/program/sales/break/up/month/two/', function(req, res){
        api_master.get_sales_bu_monthtwo(req,res);
    });

    master_route.get('/program/sales/break/up/month/three/', function(req, res){
        api_master.get_sales_bu_monththree(req,res);
    });


    // Newly added for getting top 5 ASO (Dipanjan)

    master_route.get('/get/all/aso_list', function(req, res){
        api_master.get_all_aso(req,res);
    });




    // **************** End Dashboard Dealer Normal****************************



    //**************************** Distributor Purpose*************************************//

    master_route.post('/program/details/pager', function(req, res){
        api_master.get_dets_nutshell(req,res);
    });
    master_route.get('/program/parti/details/:participantid', function(req, res){
        api_master.get_participant_details(req,res);
    });

    /********Point Details********/
    master_route.post('/points/details/distributor', function(req, res){
        api_master.get_points_details(req,res);
    });

    //*****************************************************************//



    // ******* SKU Transaction ************ //

    master_route.post('/sku/quantity/update', function(req, res){
        api_master.update_sku_quantity(req,res);
    });

    master_route.get('/search/sku/availability/:sku', function(req, res){
        api_master.search_availability_by_sku(req,res);
    });

    master_route.get('/get/sku/by/name/:name', function(req, res){
        api_master.get_sku_by_name(req,res);
    });



    // ********* Claim Services ********************

    // GET Method

    // New Added By Dipanjan For Top Dealer

    master_route.get('/get/top/dealer', function(req, res){
        api_master.get_top_dealer_by_points(req,res);
    });

    master_route.get('/get/dealer/sku_qty/wise', function(req, res){
        api_master.get_dealer_sku_qty_wise(req,res);
    });


    // End

    master_route.get('/top/si', function(req, res){
        api_master.get_top_si(req,res);
    });
    master_route.get('/top/si/by/region', function(req, res){
        api_master.get_top_si_by_region(req,res);
    });
    master_route.get('/special/program/qualifier/si', function(req, res){
        api_master.special_program_qualifier(req,res);
    });
    master_route.get('/normal/program/qualifier/si', function(req, res){
        api_master.normal_program_qualifier(req,res);
    });
    master_route.get('/si/by/region', function(req, res){
        api_master.get_si_by_region(req,res);
    });
    master_route.get('/details/by/user/:user', function(req, res){
        api_master.get_details_by_user(req,res);
    });
    master_route.get('/invoice/detail/by/id/:claim_id', function(req, res){
        api_master.get_invoice_by_number(req,res);
    });
//    master_route.get('/get/invoice/detail/by/user_id/:inv_date/:inv_no/:inv_amount', function(req, res){
//        api_master.get_invoice_by_user_id(req,res);
//    });

    master_route.post('/get/invoice/detail/by/user_id', function(req, res){
        api_master.get_invoice_by_user_id(req,res);
    });

    master_route.get('/get/invoice/details/by/user_id/:user_id', function(req, res){
        api_master.get_invoice_details_by_user_id(req,res);
    });

    master_route.get('/get/invoice/detail/by/user/:user', function(req, res){
        api_master.get_invoice_by_name(req,res);
    });
    /*New api */
    master_route.get('/get/invoice/detail/by/userid/invno/:user/:inv', function(req, res){
        api_master.get_invoice_details_by_name_inv_no(req,res);
    });

    master_route.post('/get/invoice/detail/by/userid/invno', function(req, res){
        api_master.get_invoice_details_by_name_by_inv(req,res);
    });

    master_route.post('/sku_wise/detail/by/invoice', function(req, res){
        api_master.get_sku_wise_detail_by_inv(req,res);
    });



    /* End */

    master_route.get('/get/invoice/detail/by/retailer/:retailer', function(req, res){
        api_master.get_invoice_by_retailer(req,res);
    });
    master_route.get('/search/by/sku/:sku', function(req, res){
        api_master.search_by_sku(req,res);
    });
    master_route.get('/search/sku/list/by/sku/:sku', function(req, res){
        api_master.search_sku_list(req,res);
    });
    master_route.get('/search/invoice/by/:status/:company_name', function(req, res){
        api_master.get_invoice_by_status_comp_name(req,res);
    });
    master_route.get('/claim/list/:user', function(req, res){
        api_master.get_claim_by_user(req,res);
    });
    master_route.get('/claim/submission_total/:user', function(req, res){
        api_master.get_total_submitted_value(req,res);
    });
    master_route.get('/claim/submissions/all/:user/:limit', function(req, res){
        api_master.get_all_submissions(req,res);
    });
    master_route.get('/early/bird/winner/:program_type', function(req, res){
        api_master.get_early_bird_winners(req,res);
    });

    master_route.get('/points/credited/users/:program_type', function(req, res){
        api_master.get_points_credited_users(req,res);
    });

    //Added By Hassan..Get User wise Points Data
    login_route.get('/user/wise/points/details', function(req, res){
        api_master.get_user_wise_points_details(req,res);
    });
    //Ended By Hassan.
    master_route.get('/all/schneider/users/:program_type/:company/:page_no', function(req, res){
        api_master.get_all_schneider_users(req,res);
    });
    master_route.get('/all/points/eligible/users', function(req, res){
        api_master.get_all_points_eligible_users(req,res);
    });
    master_route.get('/details/totalsale/by/user/:user', function(req, res){
       // api_master.get_user_details_for_zone_address(req,res);
       api_master.get_details_total_sale_by_user(req,res);
    });

    master_route.get('/details/zoneaddress/by/user/:user', function(req, res){
        api_master.get_all_points_eligible_users(req,res);
    });

    master_route.get('/details/goal/value/of/user/:user', function(req, res){
        api_master.get_user_goal_value(req,res);
    });

    master_route.get('/redeemed/points/of/user/:user', function(req, res){
        api_master.get_user_redeemed_points(req,res);
    });

    master_route.get('/invoice/month/proceedings/of/user/:user', function(req, res){
        api_master.get_user_proceedings_invoice_current_month(req,res);
    });

    master_route.get('/si/by/region/dashboard/:regionName', function(req, res){
        api_master.get_user_si_region_dashboard(req,res);
    });
    master_route.get('/si/by/allIndia/dashboard', function(req, res){
        api_master.get_user_si_allIndia_dashboard(req,res);
    });

    //    app.get('/si/by/normal/points',sales_registry.get_si_by_region);


   // POST Method

    master_route.post('/invoice/schneider/add', function(req, res){
        api_master.add_invoice(req,res);
    });

    master_route.post('/invoice/schneider/edit', function(req, res){
        api_master.update_invoice(req,res);
    });
    master_route.post('/invoice/claim/details/update', function(req, res){
        api_master.update_invoice_claim(req,res);
    });
    master_route.post('/search/duplicate/invoice/list', function(req, res){
        api_master.search_duplicate_invoice_list(req,res);
    });
    master_route.post('/invoice/claim/add/sku', function(req, res){
        api_master.update_claim(req,res);
    });
    master_route.post('/search/by/part/code', function(req, res){
        api_master.search_by_part_code(req,res);
    });

    master_route.post('/claim/add', function(req, res){
        api_master.add_claim(req,res);
    });
    master_route.post('/override/sku/add', function(req, res){
        api_master.add_override_sku(req,res);
    });
    master_route.post('/goal/set/by/user', function(req, res){
        api_master.posting_goal_value(req,res);
    });
    master_route.post('/goal/update/by/user', function(req, res){
        api_master.updating_goal_value(req,res);
    });


    // ********* Claim Services ********************


    // ********* SKU Services ********************

    master_route.post('/save/sku/details', function(req, res){
        api_master.add_sku_details(req,res);
    });

    master_route.get('/sku/list/:company/:region_id', function(req, res){
        api_master.get_sku_list(req,res);
    });

    master_route.get('/sku/list/all', function(req, res){
        api_master.get_all_sku_master(req,res);
    });

    //Get Monthly SKU wise sale
    login_route.get('/monthly/sku/wise/sale', function(req, res){
        api_master.get_monthly_sku_wise_sale(req,res);
    });


    login_route.get('/points/credited/users/new/:program_type', function(req, res){
        api_master.get_points_credited_users(req,res);
    });


    //app.get('/sku/list/detailed/:company/:region_id', sku.get_sku_list_detailed());
    // ********* SKU Services ********************



    // ********* User Services ********************
    master_route.get('/user/company', function(req, res){
        api_master.get_company(req,res);
    });
    master_route.get('/get/user/company/name/by/:company/:srch_key', function(req, res){
        api_master.get_user_company_name(req,res);
    });

    master_route.post('/user/level/save', function(req, res){
        api_master.save_user(req,res);
    });
    master_route.post('/user/change/password', function(req, res){
        api_master.change_password(req,res);
    });
    master_route.post('/user/terms/condition/update', function(req, res){
        api_master.terms_condition_update(req,res);
    });


    //*****************************************//

//******************************** Counter Number *************************************//

    master_route.get('/get/counter/number', function(req, res){
        api_master.get_counter_no(req,res);
    });

    master_route.post('/update/counter', function(req, res){
        api_master.update_counter_number(req,res);
    });


//**********************************************************************//

// ***************** AWS *******************************

    master_route.get('/aws/get/upload_url/:filename', function(req, res){
        api_master.get_upload_url(req,res);
    });

    master_route.post('/aws/upload/file', function(req, res){
        api_master.upload_file(req,res);
    });


    // **********************************************




    //</editor-fold>


    //</editor-fold>


    //<editor-fold desc="Last Phase">

    // implement help_route actions
    help_route.use (function(req,res,next){
        logger.info(util.create_routing_log(req.method, req.url, "help", "HELP"));
        // continue doing what we were doing and go to the route
        next();
    });
    help_route.get('/', function(req, res){
        var r_list = [];
        get_routes(ping_route.stack,r_list, "ping");
        get_routes(master_route.stack,r_list, "master");
        get_routes(help_route.stack,r_list, "help");
        res.send(r_list);
    });
    help_route.get('/show', function(req, res){
        var r_list = [];
        get_routes(ping_route.stack,r_list, "ping");
        get_routes(master_route.stack,r_list, "master");
        get_routes(help_route.stack,r_list, "help");
        //res.send(r_list);
        console.log(r_list);
        res.render('show', {'routes': r_list});
    });
    app.use('/help', help_route);
    // end help route

    //implement login_route action
    login_route.use (function(req,res,next){
        logger.info(util.create_routing_log(req.method, req.url, "login", "LOGIN"));
        // continue doing what we were doing and go to the route
        next();
    });
    //login validation for user
//    login_route.post('/validateuser', function(req, res){
//        login.validate_user(req,res);
//    });

    login_route.post('/by', function(req, res){
        api_master.user_login(req,res);
    });


    app.use('/login', login_route);
    app.use('/help', help_route);
    app.use('/master', master_route);

    //end login_route

    // This will be refactored later for adding more features and making it more generic
    var get_routes = function(r, r_list, route_sub_system){
        for (var i=0; i< r.length; i++){
            if (typeof r[i].route != "undefined"){

                r_list.push (
                    {
                        'path': "/"+ route_sub_system + r[i].route.path,
                        'method':r[i].route.methods
                    }

                )
            }
        }
        return r_list;
    }

    // and it will validate based on registered data
    function authenticate(req, res, next) {
        var token_data = token.verify(req.headers.token);
        if(token_data){
            next();
        }else{

            res.send('Not authorised');
        }
    }

    // Error handling middleware
    app.use(error_handler);

    //</editor-fold>



}