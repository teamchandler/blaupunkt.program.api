/**
 * Created by developer7 on 9/22/2014.
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var userSchema = new Schema({
    user_id        :   { type: Number },
    user_type         :   { type: String,  trim: true },
    user_first_name          :   { type: String,  trim: true },
    user_middle_name    :   { type: String,  trim: true },
    user_last_name  :   { type: String,  trim: true },
    user_zone  :   { type: String,  trim: true },
    user_name        :   { type: String,  trim: true },
    user_base_loc_id          :   { type: Number },
    user_base_loc_name  :   { type: String,  trim: true},
    user_base_loc_type  :   { type: Number},
    user_designation  :   { type: String,  trim: true },
    user_group_id        :   { type: Number },
    ref_code       :   { type: Number },
    user_email_id  :   { type: String,  trim: true },
    user_mobile  :   { type: String,  trim: true },
    user_password :   { type: String,  trim: true },
    user_salt_password :   { type: String,  trim: true },
    data_source:   { type: String,  trim: true },
    created_by  :   { type: Number },
    created_dt :   { type: Date },
    modified_by :   { type: Number },
    modified_dt :   { type: Date },
    status :   { type: Number },
    approval_status:   { type: Number },
    approval_date:   { type: Date },
    approval_by:   { type: Number }
});

var include_field = "user_id user_type user_first_name user_middle_name user_last_name user_zone user_name user_base_loc_id user_base_loc_name user_base_loc_type user_designation user_group_id ref_code user_email_id user_mobile user_password user_salt_password data_source created_by created_dt modified_by modified_dt status approval_status approval_date approval_by user_token_id";

userSchema.statics.get_user_by_name= function(user_name){
        return this.find({user_name : user_name})
            .select(include_field)
            .exec();// Should return a Promise
}

userSchema.set('collection', 'userinfo');
module.exports = mongo.get_mongoose_connection().model('userinfo', userSchema);