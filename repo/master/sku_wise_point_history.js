var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var sku_wise_point_historySchema = new Schema({
    sku          :   { type: String, trim: true },
    claim_id           :   { type: String,  trim: true },
    user_id            :   { type: String,  trim: true },
    sku_rule_id      :   { type: String,  trim: true },
    invoice_amount    :   {type: Number},
    invoice_number    :   { type: String,  trim: true },
    inv_dt_rule_id          :   { type: String,  trim: true },
    bonus_rule_id            :   { type: String,  trim: true },
    sku_wise_pt    :   { type: Number },
    inv_date_wise_pt      :   { type: Number },
    bonus_pt            :   { type: Number },
    total_calc_as_on     :   { type: Number },
    calc_date :   { type: Date },
    participant_id :      { type: Number},
    verified_amount:      { type: Number},
    name:                 { type: String,  trim: true },
    quantity:             { type: Number},
    retailer_code :{ type: String,  trim: true },
    retailer_name : { type: String,  trim: true },
    region: { type: String,  trim: true },
    distributor: { type: String,  trim: true },
    sku_verified_amount:  { type: Number},
    description: { type: String,  trim: true },
    unit_price: { type: Number},
    model: { type: String,  trim: true }



},{ versionKey: false });

var include_fields ="active claim_id user_id invoice_date invoice_amount invoice_number retailer status supporting_doc claim_details approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name  -_id";
var include_fields1 ="claim_id user_id invoice_date invoice_amount invoice_number retailer claim_details status approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name";
var include_fields2 = "claim_id user_id invoice_date invoice_amount invoice_number retailer claim_details status approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name sku quantity sku_wise_pt inv_date_wise_pt total_calc_as_on sku_verified_amount description unit_price model";
var default_sort = "invoice_date";
// static methods

sku_wise_point_historySchema.statics.get_sku_of = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $or : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'sku' : '$sku', 'model': '$model', 'description': '$description'},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"}

        }},
        {  $project:
        {_id:0,  sku: '$_id.sku',
            model: '$_id.model',
            description: '$_id.description',
            total_quantity: '$quantity',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}},
        {$limit: 5}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sku_wise_point_historySchema.statics.get_pts_of = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id'},
            total_pts_acc: {$sum: "$total_calc_as_on"}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            total_pts_acc: '$total_pts_acc'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.get_dets_nutshell = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','name':'$name','retailer_name': '$retailer_name','distributor': '$distributor'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            name : '$_id.name',
            retailer_name: '$_id.retailer_name',
            distributor:'$_id.distributor',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.get_participant_details = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {participant_id: parseInt(participantid) }]
            }
        },
        {$group: { _id: {'sku' : '$sku','model' : '$model', 'description' : '$description','name' : '$name','retailer_name':'$retailer_name'},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"}

        }},
        {  $project:
        {_id:0,  sku: '$_id.sku',
            model : '$_id.model',
            description : '$_id.description',
            name : '$_id.name',
            retailer_name: '$_id.retailer_name',
            total_quantity: '$quantity',
            total_sku_pts: '$total_sku_pts'
           }},
        {$sort: {total_sku_pts:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.post_real_total_sale_branch = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','name':'$name','retailer_name':'$retailer_name'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            name : '$_id.name',
            retailer_name:'$_id.retailer_name',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.post_real_total_sale_branch_hq = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','name':'$name','retailer_name':'$retailer_name'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            name : '$_id.name',
            retailer_name : '$_id.retailer_name',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sku_wise_point_historySchema.statics.post_real_total_sale_dealer_normal = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','name':'$name'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            name : '$_id.name',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sku_wise_point_historySchema.statics.get_ptsdetails = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {participant_id: parseInt(participantid) }]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id'},
            total_pts: {$sum: "$total_calc_as_on"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            total_pts: '$total_pts', total_count: '$total_count'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



sku_wise_point_historySchema.statics.get_saledetails = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {participant_id: parseInt(participantid) }]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id'},
            total_sale_count: {$sum: "$sku_verified_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_sale_count: '$total_sale_count', total_count: '$total_count'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sku_wise_point_historySchema.statics.post_real_total_sale_dist_performer_aso = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'distributor':'$distributor'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},

        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            distributor : '$_id.distributor',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sku_wise_point_historySchema.statics.get_dets_nutshell_aso_sales = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'distributor': '$distributor'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            distributor: '$_id.distributor',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



sku_wise_point_historySchema.statics.get_points_details = function(ids){
    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','name':'$name','retailer_name': '$retailer_name'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            name : '$_id.name',
            retailer_name: '$_id.retailer_name',
            total_sale: '$total_sale',
            total_quantity: '$quantity',
            total_count: '$total_count',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise


}


sku_wise_point_historySchema.statics.get_participant_rsmdetails = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {distributor: participantid }]
            }
        },
        {$group: { _id: {'sku' : '$sku','description' : '$description', 'name' : '$name'},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"}

        }},
        {  $project:
        {_id:0,  sku: '$_id.sku',
            description : '$_id.description',
            name : '$_id.name',
            total_quantity: '$quantity',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.get_sku_wise_detail_by_inv= function(data){

    return this.find({invoice_number : data.invoice_number})
        .select(include_fields2)
        .sort(default_sort)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.get_sku_of_all_rsm = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $or : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'sku' : '$sku', 'model' : '$model','description': '$description'},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"}

        }},
        {  $project:
        {_id:0,  sku: '$_id.sku',
            model : '$_id.model',
            description: '$_id.description',
            total_quantity: '$quantity',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.statics.get_sku_of_ho = function(){

    var pipeline =[
        {$group: { _id: {'sku' : '$sku', 'model': '$model', 'description': '$description'},
            total_sku_pts: {$sum: "$total_calc_as_on"},
            quantity: {$sum: "$quantity"}

        }},
        {  $project:
        {_id:0,  sku: '$_id.sku',
            model: '$_id.model',
            description: '$_id.description',
            total_quantity: '$quantity',
            total_sku_pts: '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}},
        {$limit: 5}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

//changed on 29th May 2015
sku_wise_point_historySchema.statics.get_disti_wise_sales_data = function(){
    var pipeline =[
        {$group: { _id: {'retailer' : '$distributor'},
            total_sale: {$sum: "$sku_verified_amount"},
            total_point: {$sum: "$total_calc_as_on"},
            total_qty: {$sum: "$quantity"},
            total_count : {$sum : 1}
        }},
        {  $project:
        {_id:0,  retailer: '$_id.retailer',
            total_sale: '$total_sale',
            total_point: '$total_point' ,
            total_qty:  '$total_qty',
            total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sku_wise_point_historySchema.set('collection', 'sku_wise_point_history')
//module.exports = mongoose.model('category', catSchema);

module.exports = mongo.get_mongoose_connection().model('sku_wise_point_history', sku_wise_point_historySchema);