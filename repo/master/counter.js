/**
 * Created by dev11 on 7/14/2015.
 */


var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var counter_schema = new Schema({
    program:{type:String,trim: true },
    id:{type:Number}
},{ versionKey: false });

var include_fields ="id _id";



counter_schema.statics.get_counter_no = function(){

    return this.find()
        .select(include_fields)
        .sort()
        .exec(); //Should return a Promise

}

counter_schema.statics.update_counter_no = function(data){

    var q ={"program":"bluerewards"};

    return this.update(
        q,
        {
            "id":data


        }
    )
        .exec();

}




counter_schema.set('collection', 'counter')
module.exports = mongo.get_mongoose_connection().model('counter', counter_schema);