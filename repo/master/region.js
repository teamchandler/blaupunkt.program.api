/**
 * Created by developer6 on 4/22/2014.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var doc_list = new Schema({
    name: { type: String, trim: true },
    img_url: { type: String, trim: true }
});

var regionSchema = new Schema({
      company : { type: String,  trim: true }
    , id: { type: String,  trim: true }
    , name: { type: String,  trim: true }
    , description: { type: String,  trim: true }
    , level:{ type: Number}
    , parent_id:{ type: String,  trim: true  }
},{ versionKey: false });

var include_fields ="company id name description level parent_id";
var default_sort = "level name"


regionSchema.statics.get_region_by_level= function(level1,level2){
   // var q = {"level" : level};
    var q={"level" :{$in :[level1,level2]}}
    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise

}

regionSchema.statics.get_region_by_id= function(id){
    var q = {"id" : {$regex : '^'+ id}};
    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise

}



regionSchema.statics.get_specific_region_by_id= function(id){
    var q = {"id" :id};
    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise

}


regionSchema.statics.get_all_region= function(){

    return this.find()
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise

}

regionSchema.set('collection', 'region')
module.exports = mongo.get_mongoose_connection().model('region', regionSchema);
