/**
 * Created by developer6 on 7/5/2014.
 */

/**

 * User: Hassan
 * Date: 10/03/14
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');



var override_skuSchema = new Schema({
    claim_id          :   { type: String, trim: true },
    user_id           :   { type: String,  trim: true },
    active            :   { type: Number },
    sku               :   { type: String,  trim: true },
    quantity            : { type: Number },
    created_date      :   { type: Date }

},{ versionKey: false });

var include_fields ="claim_id user_id active sku quantity created_date  -_id";

var default_sort = "sku";
// static methods


override_skuSchema.set('collection', 'override_sku')
//module.exports = mongoose.model('category', catSchema);

module.exports = mongo.get_mongoose_connection().model('override_sku', override_skuSchema);