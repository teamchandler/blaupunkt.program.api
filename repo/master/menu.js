/**
 * Created by rahulguha on 01/04/14.
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');
var util = require('../../util.js');


var menuSchema = new Schema({
    _id : false,
    id : { type: String, trim: true },
    Name: { type: String, trim: true },
    displayname : { type: String},
    ParentId    : { type: String}
});

var level1_menu_Schema = new Schema({
    _id : false,
    id : { type: String, trim: true },
    Name: { type: String, trim: true },
    displayname : { type: String},
    ParentId    : { type: String},
    menu_list : [menuSchema]
});
var short_include_fields ="sku Name Express brand  price thumb_link shortdesc stock description -_id";
// static methods

// for every NON SINGLETONE funtion returning data there will be two variants
// one returning all data no skip or limit
// other one will limit data by skip and limit parameter
// api will decide which one to use
// developer needs to create both the functions always
//menuSchema.statics.get_menu = function(){
//    var menu;
//    var q = {"cat_id.cat_id" : cat_id, "active": 1, "stock" : {$gt : 0}};
//    return this.find(q)
//        .select (short_include_fields)
//        .limit(limit)// app sends limit now. we can use configured limit by using util method  util.get_fetch_limit())
//        .skip (skip)
//        .exec();
//}
menuSchema.statics.get_menu1 = function() {
    var tmp = util.get_top_level_menu();
    //console.log(tmp);
    // get list of top level menu from config
    var s = [];
    for (var i = 0; i < tmp.length; i++) {
        s.push('^' +  tmp[i].id.substr(0,tmp[i].id.length -1  ));
    };
//    var q = {active: 1,  $or: [
//        {id: { $in: s}},
//        {ParentId: { $in : s}}]
//    };
//    var q = {active: 1, id:  { $regex :  "^2-3"} } ;
//    var q = {active: 1, "id":  { "$regex" : '^2.' } };
    var q = {active: 1, "id":  { "$regex" : { "$in" : s }}};
    //var q = {active: 1, "id":  { "$regex" : "^\d{1}[..]" }} ;

//        {id: { $in: s}},
//        {ParentId: { $in : s}}]
//    };

    //db.category.find(q);

//        .exec();
    console.log(s);
    console.log(q);
    return this.find(q)
        .select ("id Name displayname ParentId -_id")
        .sort ("id ParentId")
        .exec();
}
menuSchema.statics.get_menu = function() {
    var tmp = util.get_top_level_menu();
    //console.log(tmp);
    // get list of top level menu from config
    var s = [];
    for (var i = 0; i < tmp.length; i++) {
        s.push(tmp[i].id);
    };
    var q = {active: 1,  $or: [
        {id: { $in: s}},
        {ParentId: { $in : s}}]
    };
//    var q = {active: 1, id:  { $regex :  "^2-3"} } ;
//    var q = {active: 1, "id":  { "$regex" : '^2.' } };
//    var q = {active: 1, "id":  { "$regex" : { "$in" : s }} };
    //var q = {active: 1, "id":  { "$regex" : "^\d{1}[..]" }} ;

//        {id: { $in: s}},
//        {ParentId: { $in : s}}]
//    };

    //db.category.find(q);

//        .exec();
//    console.log(s);
//    console.log(q);
    return this.find(q)
        .select ("id Name displayname ParentId -_id")
        .sort ("id ParentId")
        .exec();
}

menuSchema.set('collection', 'category')
module.exports = mongo.get_mongoose_connection().model('menu', menuSchema);