/**
 * Created by DRIPL_1 on 03/24/2015.
 */


var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var sku_tran_schema = new Schema({
    sku: { type: String,  trim: true },
    frm_participant :{ type: String,  trim: true },
    to_participant : { type: String,  trim: true },
    from_participant_code:{ type: String,  trim: true },
    to_participant_code:{ type: String,  trim: true },
    uploaded_by_participant_code:{type: String,  trim: true},
    uploaded_by_participant:{type: String,  trim: true},
    invoice_number:{type: String,  trim: true},
    flow_type: { type: Number },
    tran_type : { type: String,  trim: true },
    quantity: { type: Number },
    credit_quantity: { type: Number },
    debit_quantity:  { type: Number },
    active :{ type: Number },
    created_date : { type: Date }
},{ versionKey: false });

var include_fields ="frm_participant to_participant flow_type tran_type quantity credit_quantity debit_quantity active created_date";
var short_include_fields ="";
var default_sort = ""



sku_tran_schema.statics.search_availability_by_sku = function(sku){

//    var q="{sku: /^"+ sku +"$/i}";
    var q={'sku' : sku.toUpperCase()};

    console.log(q);

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise
}



sku_tran_schema.set('collection', 'sku_wise_tran')
module.exports = mongo.get_mongoose_connection().model('sku_wise_tran', sku_tran_schema);
