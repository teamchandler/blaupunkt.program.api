/**
 * Created by rahulguha on 25/04/14.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var sku_schema = new Schema({
    brand : { type: String,  trim: true },
    sku: { type: String,  trim: true },
    description : { type: String,  trim: true },
    active: { type: Number },
    model:{ type: String,  trim: true },
    mrp:{ type: Number },
    bluerewards: {type: Number }
    },{ versionKey: false });

var include_fields ="brand sku description model active mrp bluerewards";
var include_fields1 = "sku";
var short_include_fields ="brand sku  description active";
var default_sort = "sku"


sku_schema.statics.get_sku_list= function(company,region_id){
    var pipeline = [{$unwind : "$region_value"},
                    {$match  : {$and : [
                            {"region_value.id": region_id},
                            {"company": company},
                            {"active": 1}
                        ]
                    }},
                    {$project: {_id:0, sku: 1, "value" : "$region_value.value"}},
                    {$sort   : {sku : 1} }
        ];
    return this.aggregate (pipeline)
         .exec();// Should return a Promise
}

sku_schema.statics.get_all_sku_master = function(){
    var query = {active:1};
    return this.find(query)
        .select(include_fields)
        //.sort(default_sort)
        .exec();// Should return a Promise
}

sku_schema.statics.search_by_sku = function(sku){

//    var q="{sku: /^"+ sku +"$/i}";
    var q={'sku' : sku.toUpperCase()};

    console.log(q);

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise
}


sku_schema.statics.search_by_part_code = function(sku){


    var q={'sku' : sku.toUpperCase()};

    console.log(q);

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise


}
sku_schema.statics.get_sku_by_name = function(model){

    var q={'model':model};

    console.log(q);

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise


}




sku_schema.statics.search_sku_list = function(sku){

    var q={};
//    if(sku!='undefined' && sku!='')
//    {
//        q="{ sku: { $regex: "+ sku.toUpperCase()+", $options : 'i' }}";
//    }

//    q="{sku: /^"+ sku.toUpperCase() +"/i}";


//    console.log(q+"hello");

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise
}



//sku_schema.statics.get_detailed_sku_list= function(company,region_id){
//    var pipeline = [
//        {$match  : {$and : [
//            {"region_value.id": region_id},
//            {"company": company},
//            {"active": 1}
//        ]'
//        }},
//        {$project: {_id:0, sku: 1, company: 1, company_id: 1, "value" : "$region_value.value"}},
//        {$sort   : {sku : 1} }
//    ];
//    return this.aggregate (pipeline)
//        .exec();// Should return a Promise
//}

sku_schema.set('collection', 'sku_master')
module.exports = mongo.get_mongoose_connection().model('sku_master', sku_schema);