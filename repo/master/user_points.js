/**
 * Created by rahulguha on 7/8/14.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var user_pointsSchema = new Schema({

    user_id         :   { type: String,  trim: true },
    point           :   { type: Number },
    txn_type        :   { type: String,  trim: true },
    ts              :   { type: Date }

},{ versionKey: false });



user_pointsSchema.statics.get_points_credited_users = function(program_type){
    var pipeline =
        [
            {$match : { txn_type: 'c' }},
            {$group: {_id: {'user_id': '$user_id'}, "point_received" : {$sum : "$point"}}},
            {$project: {
                _id: 0, "user_id" : "$_id.user_id" , "point_received": 1}}
        ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


user_pointsSchema.set('collection', 'user_points')

module.exports = mongo.get_mongoose_connection().model('user_points', user_pointsSchema);