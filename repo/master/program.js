/**
 * Created by developer6 on 4/22/2014.
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var programSchema = new Schema({
      company : { type: String,  trim: true }
    , id : { type: String,  trim: true }
    , name: { type: String,  trim: true }
    , description: { type: String,  trim: true }
    , region: { type: String,  trim: true }
    , rule:{
        rule_type :{ type: String,  trim: true },
        sku:{ type: String,  trim: true },
        volume:{ type: String,  trim: true },
        value:{ type: String,  trim: true },
        incentive_type:{ type: String,  trim: true },
        point:{ type: String,  trim: true }
    }
    , start_date:{ type: Date }
    , end_date:{ type: Date }
},{ versionKey: false });


var include_fields ="id name description region rule start_date end_date company ";
var default_sort = "start_date"


programSchema.statics.program_by_date= function(start_date,end_date){
    //var claims = [];
    return this.find({start_date : start_date, end_date : end_date})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}
programSchema.statics.get_program_by_company_region= function(company, region, date){
    //var claims = [];
    var pipeline = [{$unwind : "$region"},
        {$match  : {$and : [
            {"region": region},
            {"company": company},
            {"active": 1},
            {'start_date' :{$lte : date}}
        ]
        }},
        {$sort : {start_date : -1}},
        {$project: {_id:0, name : 1, id : 1, rule : 1}},
        {$limit : 6}
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

programSchema.statics.get_all_program = function(){
    //var claims = [];
    return this.find()
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}

programSchema.statics.get_program_by_id = function(id){
    //var claims = [];
    return this.find({id :id})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}

programSchema.statics.program_by_region= function(region){
    //var claims = [];
    return this.find({region : region})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}
programSchema.set('collection', 'program')
module.exports = mongo.get_mongoose_connection().model('program', programSchema);
