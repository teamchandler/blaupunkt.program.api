/**
 * Created by rahulguha on 26/04/14.
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var region_schema = new Schema({
    "region" : { type: String, trim: true },
    _id : 0
});
var approver_list_schema = new Schema({
    "approver_list" : { type: String, trim: true },
    _id : 0
});
var steps_schema = new Schema({
    "step_name" : { type: String, trim: true },
    "sortorder" : { type: Number },
    "num_approval" : { type: Number},
    "approver_list" : [ approver_list_schema],
    "approver_source" :{ type: String, trim: true },
    "approval_mode" : { type: String, trim: true },
    _id : 0
});
var workflow_schema = new Schema({

    "name" : { type: String, trim: true },
    "company" :{ type: String, trim: true },
    "active" : { type:Number },
    "region" : [region_schema ],
    "steps" : [steps_schema]

     });


workflow_schema.statics.get_workflow_by_company_region= function(company,region){
    var pipeline = [ {$unwind: "$region"},
        {$match: {$and : [ {company : company}, { active : 1}, { region : region}] }},
        {$project : {_id:0, name: 1, steps:1}},
        {$unwind : "$steps"},
        {$sort : {"steps.sortorder" : 1}}
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


workflow_schema.set('collection', 'workflow_definition')
module.exports = mongo.get_mongoose_connection().model('workflow_definition', workflow_schema);