/**

 * User: Rahul
 * Date: 31 March 2014
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');
var util = require('../../util.js');


var productSchema = new Schema({
    _id : false,
    Express : { type: String, trim: true },
    Name: { type: String, trim: true },
    active : { type: Number},
    brand: { type: String, trim: true },
    create_date : { type: Date},
    description :  { type: String, trim: true },
    id :  { type: String, trim: true },
    cat_id : [{type: String}],
    parent_prod_id :  { type: String, trim: true },
    parent_prod :  { type: String, trim: true },
    shortdesc :  { type: String, trim: true },
    sku :  { type: String, trim: true },
    thumb_link :  { type: String, trim: true },
    stock : {type: Number},
    price : {
        mrp: { type: Number  },
        list : { type: Number  },
        discount : { type: Number  },
        min : { type: Number  },
        final_offer : { type: Number  },
        final_discount : { type: Number  }
    }

});

var short_include_fields ="sku Name Express brand  price image_urls.thumb_link shortdesc stock  -_id";
var default_sort = "cat_id.cat_id Name price"
// static methods

// for every NON SINGLETONE funtion returning data there will be two variants
// one returning all data no skip or limit
// other one will limit data by skip and limit parameter
// api will decide which one to use
// developer needs to create both the functions always
productSchema.statics.get_prod_by_cat_id = function(cat_id, limit, skip){
    var q = {"parent_prod" : "0", "cat_id.cat_id" : cat_id, " active": 1, "stock" : {$gt : 0}}
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)// app sends limit now. we can use configured limit by using util method  util.get_fetch_limit())
        .sort(default_sort)
        .skip (skip)
        .exec();
}
productSchema.statics.get_prod_by_cat_id_no_limit = function(cat_id){
    var q = {"parent_prod" : "0", "cat_id.cat_id" : cat_id, "active": 1, "stock" : {$gt : 0}};

    return this.find(q)
        .select (short_include_fields)
        .sort(default_sort)
        .exec();
}
productSchema.statics.get_prod_by_level1_cat_id_no_limit = function(cat_id){
    var q = {"parent_prod" : "0", "cat_id.cat_id" : { $regex : "^"  +  cat_id.substr(0, cat_id.length-1 ) } , "active": 1, "stock" : {$gt : 0}};

    return this.find(q)
        .select (short_include_fields)
        .sort(default_sort)
        .exec();
}

productSchema.statics.get_prod_by_level1_cat_id = function(cat_id, limit, skip){
    var q = {"parent_prod" : "0", "cat_id.cat_id" : { $regex : "^"  +  cat_id.substr(0, cat_id.length-1 ) } , "active": 1, "stock" : {$gt : 0}};
    console.log(q);
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)
        .skip(skip)
        .sort(default_sort)
        .exec();
}

//added by arnab
productSchema.statics.get_prod_by_spcat_range_no_limit = function(cat_id,greater_than,less_than){
    var q = { "cat_id.cat_id" : cat_id ,  "price.list" : {$gt:greater_than} ,"price.list" :{$lt:less_than}, "active": 1,  "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .sort(default_sort)
        .exec();
}

productSchema.statics.get_prod_by_spcat_range = function(cat_id,greater_than,less_than, limit, skip){
    var q = { "cat_id.cat_id" : cat_id,"price.list" : {$gt:greater_than} ,"price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)
        .skip(skip)
        .sort(default_sort)
        .exec();
}

// added by arnab Get product list by price range
productSchema.statics.get_prod_by_range = function(greater_than,less_than,limit, skip){
    var q = {"price.list" : {$gt:greater_than},"price.list" : {$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)// app sends limit now. we can use configured limit by using util method  util.get_fetch_limit())
        .skip (skip)
        .exec();
}


productSchema.statics.get_prod_by_range_no_limit = function(greater_than,less_than){
    var q = { "price.list" : {$gt:greater_than} ,"price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .exec();
}


// added Get product list below certain price range

productSchema.statics.get_prod_by_below_certain_price_range = function(less_than,limit, skip){
    var q = {"price.list" : {$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)// app sends limit now. we can use configured limit by using util method  util.get_fetch_limit())
        .skip (skip)
        .exec();
}


productSchema.statics.get_prod_by_below_certain_price_range_no_limit = function(less_than){
    var q = { "price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .exec();
}

//arnab added Get product list for specific top level cat and price range
productSchema.statics.get_prod_by_level1_cat_id_price_range = function(cat_id,greater_than,less_than,limit, skip){
    var q = {"cat_id.cat_id" : { $regex : "^"  +  cat_id.substr(0, cat_id.length-1 ) } ,"price.list" : {$gt:greater_than} ,"price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};

    return this.find(q)
        .select (short_include_fields)
        .limit(limit)
        .skip(skip)
        .sort(default_sort)
        .exec();
}

productSchema.statics.get_prod_by_level1_cat_id_price_range_no_limit= function(cat_id,greater_than,less_than){
    var q = {"cat_id.cat_id" : { $regex : "^"  +  cat_id.substr(0, cat_id.length-1 ) } ,"price.list" : {$gt:greater_than} ,"price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    console.log(q);
    return this.find(q)
        .select (short_include_fields)

        .sort(default_sort)
        .exec();
}

//arnab added Get product list for specific top level cat and less than certain range




productSchema.statics.get_prod_by_level_cat_id_lessthan_price_range = function(cat_id,less_than,limit, skip){
    var q = {"cat_id.cat_id" : { $regex : "^"  +  cat_id.substr(0, cat_id.length-1 ) } ,"price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};

    return this.find(q)
        .select (short_include_fields)
        .limit(limit)
        .skip(skip)
        .sort(default_sort)
        .exec();
}

productSchema.statics.get_prod_by_level_cat_id_lessthan_price_range_no_limit= function(cat_id,less_than){
    var q = {"cat_id.cat_id" : { $regex : "^"  +  cat_id.substr(0, cat_id.length-1 ) } ,"price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};
    console.log(q);
    return this.find(q)
        .select (short_include_fields)

        .sort(default_sort)
        .exec();
}

//arnab added for express
productSchema.statics.get_prod_by_express  = function(Express,limit, skip){
    var q = {"Express" : "1", "active": 1, "stock" : {$gt : 0}}
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)// app sends limit now. we can use configured limit by using util method  util.get_fetch_limit())
        .sort(default_sort)
        .skip (skip)
        .exec();
}
productSchema.statics.get_prod_by_express_no_limit = function(Express){
    var q = {"Express" : "1", "active": 1, "stock" : {$gt : 0}};

    return this.find(q)
        .select (short_include_fields)
        .sort(default_sort)
        .exec();
}


//added arnab added for express below certain price

productSchema.statics.get_prod_by_express_range = function(Express,less_than,limit, skip){
    var q = {"Express" : "1", "price.list" :{$lt:less_than},"active": 1, "stock" : {$gt : 0}};
    return this.find(q)
        .select (short_include_fields)
        .limit(limit)// app sends limit now. we can use configured limit by using util method  util.get_fetch_limit())
        .sort(default_sort)
        .skip (skip)
        .exec();
}
productSchema.statics.get_prod_by_express_range_no_limit = function(Express,less_than){
    var q = {"Express" : "1", "price.list" :{$lt:less_than}, "active": 1, "stock" : {$gt : 0}};

    return this.find(q)
        .select (short_include_fields)
        .sort(default_sort)
        .exec();
}

//sku
productSchema.statics.find_product_by_sku = function(sku){
    //var claims = [];
    return this.find({sku : sku}).exec();// Should return a Promise
}

//sku

//
productSchema.statics.find_product_by_sku = function(sku){
    //var claims = [];
    return this.find({sku : sku}).exec();// Should return a Promise
}

//sku

//

productSchema.set('collection', 'product')
module.exports = mongo.get_mongoose_connection().model('products', productSchema);