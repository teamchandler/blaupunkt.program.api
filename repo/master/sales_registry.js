
/**

 * User: Hassan
 * Date: 10/03/14
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var sku_schema = new Schema({
    "sku" : { type: String, trim: true },
    "status": { type: String, trim: true },
    "quantity": {type: Number , trim: true},
    "description" : { type: String, trim: true },
    "unit_price" : {type: Number, trim: true },
    "amount":{type: Number, trim: true },
    "discount":{type: Number, trim: true },
    "is_free":{type: Number, trim: true },
    "model": { type: String,  trim: true},
    _id : 0
});

var approve_schema = new Schema({
    "comments" : { type: String, trim: true },
    "user_type": { type: String, trim: true },
    "user_id": { type: String, trim: true },
    "comment_date": { type: Date },
    _id : 0
});

var amount_schema = new Schema({
    user_id  :        { type: String,  trim: true },
    invoice_amount  : {type: Number},
    track_date    :   { type: Date },
    verified_amount : {type: Number},
    _id :0

});


var inv_date_schema = new Schema({
    user_id       :   { type: String,  trim: true },
    inv_date      :   { type: Date },
    track_date    :   { type: Date },
    _id :0

});


var sales_registrySchema = new Schema({
    claim_id          :   { type: String, trim: true },
    user_id           :   { type: String,  trim: true },
    active            :   { type: Number },
    invoice_date      :   { type: Date },
    invoice_amount    :   {type: Number},
    invoice_number    :   { type: String,  trim: true },
    retailer          :   { type: String,  trim: true },
    status            :   { type: String,  trim: true },
    supporting_doc    :   { type: String,  trim: true },
    program_name      :   { type: String,  trim: true },
    region            :   { type: String,  trim: true },
    claim_details     :   [sku_schema],
    approval_comments :   [approve_schema],
    amount_track      :   [amount_schema],
    inv_date_track    :   [inv_date_schema],
    company_name      :   { type: String,  trim: true },
    created_date      :   { type: Date },
    modified_by       :   { type: String,  trim: true },
    modified_date     :   { type: Date },
    ispoint_calculated:   { type: Number},
    retailer_code  :      { type: String,  trim: true },
    retailer_name  :      { type: String,  trim: true },
    participant_id :      { type: Number},
    verified_amount:      { type: Number},
    upload_from:          { type: String,  trim: true},
    name:                 { type: String,  trim: true},
    distributor_code:     { type: String,  trim: true},
    state:                { type: String,  trim: true},
    total_cal_pt    :      { type: Number}


},{ versionKey: false });

var include_fields ="active claim_id user_id invoice_date invoice_amount invoice_number retailer status supporting_doc claim_details approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name distributor_code retailer_name total_cal_pt -_id ";
var include_fields1 ="claim_id user_id invoice_date invoice_amount invoice_number retailer claim_details status approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name distributor_code retailer_name retailer_code participant_id name";
var default_sort = "invoice_date";
// static methods

sales_registrySchema.statics.update_invoice = function(data){



    var q ={"claim_id": data.claim_id};

    return this.update(
        q,
        {
            "user_id": data.user_id,
            "company": data.company,
            "active": data.active,
            "invoice_number": data.invoice_number,
            "retailer": data.retailer,
            "status": data.status,
            "approval_comments": data.approval_comments


        }
                      )
    .exec();
}

sales_registrySchema.statics.get_invoice_by_name= function(user){

    return this.find({user_id : user , active: 1})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_invoice_details_by_name_inv_no= function(data){

    return this.find({user_id : data.user ,invoice_number:data.inv})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_invoice_details_by_name_by_inv= function(data){

    return this.find({user_id : data.user_id ,invoice_number:data.invoice_number})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}



sales_registrySchema.statics.get_all_invoice= function(inv,retailer){

    return this.find({invoice_number:inv,retailer:retailer,status :"Verified & Approved"})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_invoice_by_retailer= function(retailer){

    return this.find({retailer : retailer})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
}



sales_registrySchema.statics.search_duplicate_invoice_list = function(invoice_number){

//    var q="{sku: /^"+ sku +"$/i}";
    var q={'invoice_number' : invoice_number};

    console.log(q);

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise
}

sales_registrySchema.statics.update_invoice_claim = function(data, prog_name){
    var q ={"claim_id": data.claim_id };

    console.log(prog_name);
    var  upd_qry=
    {
        "claim_details": data.claim_details,
        "status": data.status,
        "approval_comments":data.approval_comments,
        "invoice_amount":data.invoice_amount,
        "amount_track" : data.amount_track,
        "inv_date_track" : data.inv_date_track,
        "modified_by" : data.modified_by,
        "modified_date" : data.modified_date,
        "program_name":prog_name,
        "invoice_date":data.invoice_date,
        "verified_amount" :data.verified_amount

    };
    if(prog_name==""){
        delete upd_qry.program_name;
        delete upd_qry.invoice_date;
    }

    return this.update(
        q,upd_qry
    )
        .exec();
}

sales_registrySchema.statics.get_claim_by_user = function(user_id){
    var pipeline = [
        {$match :
        {$and : [
            {active : 1},{user_id : user_id},
            {'status' : 'pending'}
        ]
        }
        }
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_early_bird_winners = function(program_type){
    var pipeline = [
        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved' },
                {program_name: 'nrml'},
                {invoice_date:    { $gte: new Date( '06/01/2014' )}},
                {invoice_date:    { $lte: new Date( '07/31/2014' )}}
            ]

        }},
        {$group : {     _id: {'user_id': '$user_id', 'company' : '$company_name'},
            'total_sale' : {$sum: '$invoice_amount'}}},
        {$project: {
            _id: 0, "company" : "$_id.company",
            "user_id" : "$_id.user_id" , "total_sale": 1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}




sales_registrySchema.statics.get_invoice_by_status_comp_name = function(status,company_name){

    var  q ={active : 1 , status : status , company_name : company_name};

    if(status=="All"){
        delete q.status;
    }
    if(company_name=="" || company_name==null|| company_name=='undefined') {
        delete q.company_name;
    }

    console.log(q);

    return this.find(q)
        .select(include_fields)
        // .sort(default_sort)
        .exec();// Should return a Promise

}

sales_registrySchema.statics.update_claim = function(data){
    var q ={"claim_id": data.claim_id};

    return this.update(
        q,
        {
            "claim_details": data.claim_details

        }
    )
        .exec();
}

sales_registrySchema.statics.get_invoice_by_number= function(number){

    return this.find({claim_id : number})
        .select(include_fields1)
        // .sort(default_sort)
        .exec();// Should return a Promise
}





sales_registrySchema.statics.get_invoice_by_user_id= function(date,inv_no,inv_amount){

    //var p = {"invoice_number":new RegExp(inv_no ,'i'),"invoice_date":date,"invoice_amount":inv_amount};
    var p = {"invoice_number":inv_no,"invoice_date":date,"invoice_amount":inv_amount};


    console.log(p);

  //  var q_data ={invoice_number:"/^"+ inv_no +"/"};

   // return this.find({invoice_number:{ $regex: /^"+ inv_no +"/  ,$options: 'i'}})
    return this.find(p)
        .select(include_fields1)
        // .sort(default_sort)
        .exec();// Should return a Promis
}


sales_registrySchema.statics.get_invoice_details_by_user_id= function(user_id){

    var p = {"user_id":user_id,active: 1};


    console.log(p);

    return this.find(p)
        .select(include_fields1)
        // .sort(default_sort)
        .exec();// Should return a Promis
}



//Schneider Reports

sales_registrySchema.statics.get_top_si = function(){
var pipeline =[
    {
        $match: { $and : [
            {active: 1},
            {status: 'Verified & Approved'}]
        }
    },
    {$group: { _id: "$retailer",
        total_sale: {$sum: "$invoice_amount"}
    }},
    {$project: {_id:0, retailer: '$_id', total_sale: '$total_sale'}},
    {$sort: {total_sale:-1}},
    {$limit: 15}

];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



sales_registrySchema.statics.get_top_si_by_region = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'}]
            }
        },
        {$group: { _id: {'region' : '$region', 'SI' : "$company_name"},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  region: '$_id.region', 'SI' : '$_id.SI',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_si_by_region = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'},
                {region: { $in: ['East', 'West','North' , 'South'  ] }}]
            }
        },
        {$group: { _id: {'region' : '$region'},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  region: '$_id.region',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_user_proceedings_invoice_current_month = function(user){
    var pipeline =[
        {
            $match: { $and : [
                {invoice_date:    { $gte: new Date( '12/01/2014' )}},
                {invoice_date:    { $lte: new Date( '12/29/2014' )}},
                {user_id: user}
            ]
            }

        },

        /*{$group: { _id: {'user_id' : '$user_id'},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},*/
        {$project: {_id:0, companyname: '$company_name', invoiceAmount: '$invoice_amount', invoiceDate: '$invoice_date'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.special_program_qualifier = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'},
                {program_name : 'spl1'}
            ]
            }
        },
        {$group: { _id: '$user_id',
            total_sale: {$sum: "$invoice_amount"}
        }},
        {
            $match: {total_sale : {$gte : 100000}}
        },
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.normal_program_qualifier = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'},
                {program_name : 'nrml'}
            ]
            }
        },
        {$group: { _id: '$user_id',
            total_sale: {$sum: "$invoice_amount"}
        }},
        {
            $match: {total_sale : {$gte : 100000}}
        },
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_all_users = function(){


    var pipeline = [
        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved' }

            ]

        }},
        {$group: {_id: '$user_id', 'total_sale' : {$sum: '$invoice_amount'}}}
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise


}
sales_registrySchema.statics.get_details_by_user = function(user_id){
    var pipeline =[
        {$match: { $and : [
            {active: 1},
            {status: 'Verified & Approved'},
            {"user_id" : user_id}
        ]
        }
        },
        {$group: { _id: {'program_name' : '$program_name', 'user_id' : "$user_id"},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {$project:
        {_id:0,  program_name: '$_id.program_name', 'user_id' : '$_id.user_id',
            total_sale: '$total_sale', total_count: '$total_count'}},

        {$sort: {'user_id':1}}



    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_sku_of = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $or : [
                {participant_id: { $in: barray }}]
            }
        },
        {  $project: {_id: 0, claim_details: '$claim_details',cds: '$claim_details.quantity' }},
        {$sort: {cds:-1}},
        {$limit: 3}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_dets_nutshell = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {  $project: {_id: 0, name: '$name', verified_amount: '$verified_amount', claim_details: '$claim_details',  participant_id: '$participant_id'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_participant_details = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {participant_id: parseInt(participantid) }]
            }
        },
        {  $project: {_id: 0, name: '$name', claim_details: '$claim_details',  participant_id: '$participant_id'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



sales_registrySchema.statics.get_user_wise_total_claim = function(user_email){

    var pipeline = [
        {$match: { $and : [
            {active: 1},
            {status: 'Verified & Approved'},
            {user_id : user_email}]
        }
        },
        {$group: { _id: {'program_name' : '$program_name', 'user_id' : "$user_id"},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {$project:
        {_id:0,  program_name: '$_id.program_name', 'user_id' : '$_id.user_id',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise


}

/**Author Subhajit Modified Date : 25th Dec 2014**/
/** The definition of the schema in DB above. Fetches the function get_details_total_sale_by_user from the router of sales registry
 *
 * @param user_id
 * @returns {Promise}
 */

sales_registrySchema.statics.get_details_total_sale_by_user = function(user_id){
    var pipeline =[
        {$match: { $and : [
            {active: 1},
            {"user_id" : user_id}
        ]
        }
        },
        {$group: { _id: {'status' : '$status', 'user_id' : "$user_id"},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {$project:
        {_id:0,  status: '$_id.status', 'user_id' : '$_id.user_id',
            total_sale: '$total_sale', total_count: '$total_count'}}



    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_user_si_region_dashboard = function(regionName){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {region: regionName},
                {status: 'Verified & Approved'}]
            }
        },
        {$group: { _id: {'region' : '$region', 'SI' : "$company_name", 'user_id' : "$user_id"},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  region: '$_id.region', 'SI' : '$_id.SI', 'user_id' : '$_id.user_id',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_user_si_allIndia_dashboard = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'}]
            }
        },
        {$group: { _id: {'region' : '$region', 'SI' : "$company_name", 'user_id' : "$user_id"},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  region: '$_id.region', 'SI' : '$_id.SI', 'user_id' : '$_id.user_id',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



sales_registrySchema.statics.post_real_total_sale_branch = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','retailer' : '$retailer'},
            total_sale: {$sum: "$verified_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.post_real_total_sale_branch_hq = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','retailer' : '$retailer'},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.post_real_total_sale_dealer_normal = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id','retailer' : '$retailer'},
            total_sale: {$sum: "$invoice_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_pts_of = function(ids){

    var myarray = ids;
    var barray = [];
    for(var i=0;i<myarray.length;i++)
    {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline =[
        {
            $match: { $and : [
                {participant_id: { $in: barray }}]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id'},
            total_sale: {$sum: "$total_cal_pt"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_sale: '$total_sale', total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_ptsdetails = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {participant_id: parseInt(participantid) }]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id'},
            total_pts: {$sum: "$total_cal_pt"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_pts: '$total_pts', total_count: '$total_count'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_saledetails = function(participantid){


    var pipeline =[
        {
            $match: { $and : [
                {participant_id: parseInt(participantid) }]
            }
        },
        {$group: { _id: {'participant_id' : '$participant_id'},
            total_sale_count: {$sum: "$verified_amount"},
            total_count : {$sum : 1}

        }},
        {  $project:
        {_id:0,  participant_id: '$_id.participant_id',
            'retailer' : '$_id.retailer',
            total_sale_count: '$total_sale_count', total_count: '$total_count'}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



//sales_registrySchema.statics.get_all_skus = function(){
//
//    var pipeline = [
//        {$match: {$and :
//            [
//                {active:1},
//                {status : 'Verified & Approved'}
//            ]
//
//        }},
//        {$unwind:  '$claim_details'},
//        { $group: { _id: "$claim_details.sku", total_qty: { $sum:"$claim_details.quantity" } } }
//    ];
//    return this.aggregate (pipeline)
//        .exec();// Should return a Promise
//}



 sales_registrySchema.statics.get_all_skus = function(){

    var pipeline = [
        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved'}
            ]

        }},
        {$unwind:  '$claim_details'},
        { $group: { _id:{sku: "$claim_details.sku",description:"$claim_details.description",model: "$claim_details.model"} , total_qty: { $sum:"$claim_details.quantity" } } },
        {  $project:
        {  _id: '$_id.sku',
            'description' : '$_id.description',
            'model' : '$_id.model',
            total_qty: '$total_qty'
        }}
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
   }




sales_registrySchema.statics.get_month_wise_sku_qty = function(sku, month ){

    var pipeline = [ {$match: {$and :
        [
            {active:1},
            {status : 'Verified & Approved'}
        ]
    }},
        {$project: { user_id: '$user_id' , invoice_number:  "$invoice_number", claim_details : "$claim_details", month: {$month: '$invoice_date'}}},
        {$match: {$and :
            [
                {month: month},
                {claim_details: {"$elemMatch": {sku: sku}}}
            ]
        }}];

   //  console.log(pipeline);

    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_today_sku_qty = function(sku,year,month,day ){

    var pipeline = [ {$match: {$and :
        [
            {active:1},
            {status : 'Verified & Approved'}
        ]

    }},
        {$project: { user_id: '$user_id' , invoice_number:  "$invoice_number",
            claim_details : "$claim_details",
            year:  {$year: '$modified_date'},
            month: {$month: '$modified_date'},
            day:   {$dayOfMonth: '$modified_date'}

        }},
        {$match: {$and :
            [
                {year: year},
                {month: month},
                {day: day},
                {claim_details: {"$elemMatch": {sku: sku}}}
            ]
        }}];

//       console.log(pipeline);

    return this.aggregate (pipeline)
        .exec();// Should return a Promise


}


//Added By Hassan Ahamed
//sales_registrySchema.statics.get_disti_wise_sales_data = function(){
//    var pipeline =[
//        {
//            $match: { $and : [
//                {active: 1},
//                {status: 'Verified & Approved'},
//                {ispoint_calculated : 0}]
//            }
//        },
//        {$group: { _id: {'retailer' : '$retailer'},
//            total_sale: {$sum: "$verified_amount"},
//            total_point: {$sum: "$total_cal_pt"},
//            total_count : {$sum : 1}
//        }},
//        {  $project:
//        {_id:0,  retailer: '$_id.retailer',
//            total_sale: '$total_sale',
//            total_point: '$total_point' ,
//            total_count: '$total_count'}},
//        {$sort: {total_sale:-1}}
//
//    ];
//    return this.aggregate (pipeline)
//        .exec();// Should return a Promise
//}


sales_registrySchema.statics.get_top_dealer_by_points = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'},
                {ispoint_calculated:0}]
            }
        },
        {$group: { _id: "$retailer_name",
            total_point: {$sum: "$total_cal_pt"}
        }},
        {$project: {_id:0, retailer: '$_id', total_point: '$total_point'}},
        {$sort: {total_point:-1}},
        {$limit: 5}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_dealer_sku_qty_wise = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'}]
            }
        },
        {$unwind:  '$claim_details'},
        { $group: { _id: "$retailer_name", total_quantity: { $sum:"$claim_details.quantity"}
        }},
        {$project: {_id:0, retailer: '$_id', quantity:'$total_quantity'}},
        {$sort: {quantity:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



//Added By Hassan Start

sales_registrySchema.statics.get_user_wise_sku_points = function(){

    var pipeline = [
        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved'},
                {ispoint_calculated:0}
            ]
        }},
        {$group: { _id: {'user_id' : '$user_id'},
            total_sale: {$sum: "$verified_amount"},
            total_sku_pts: {$sum: "$total_cal_pt"},
            total_count : {$sum : 1}
        }},
        {  $project:
        {_id:0,  user_id: '$_id.user_id',
            total_sale : '$total_sale',
            total_sku_pts : '$total_sku_pts'}},
        {$sort: {total_sku_pts:-1}}
    ];

//       console.log(pipeline);

    return this.aggregate (pipeline)
        .exec();// Should return a Promise


}
//End Hassan Ahamed


sales_registrySchema.statics.get_disti_wise_sku_sales_data = function(){
    var pipeline =[
        {
            $match: { $and : [
                {active: 1},
                {status: 'Verified & Approved'},
                {ispoint_calculated : 0}]
            }
        },
        {$unwind: '$claim_details'},
        {$group: { _id: {'retailer' : '$retailer'},
            total_sale: {$sum: "$verified_amount"},
            total_point: {$sum: "$total_cal_pt"},
            total_qty: {$sum: "$claim_details.quantity"},
            total_count : {$sum : 1}
        }},
        {  $project:
        {_id:0,  retailer: '$_id.retailer',
            total_sale: '$total_sale',
            total_point: '$total_point' ,
            total_qty:  '$total_qty',
            total_count: '$total_count'}},
        {$sort: {total_sale:-1}}

    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

//End By Hassan Ahamed

sales_registrySchema.statics.get_sales_bu = function(){
    var pipeline =[
//        {$match: {$and :
//            [
//                {active:1},
//                {status : 'Verified & Approved' },
//                {ispoint_calculated : 0} ,
//                {invoice_date:    { $gt:  new Date("2015-03-31T18:30:00.000Z")}},
//                {invoice_date:    { $lte:  new Date("2015-04-30T18:30:00.000Z")}}
//            ]
//
//        }},
//        {$group : {     _id: {'total_sale': '$total_sale'},
//            'total_sale' : {$sum: '$verified_amount'}}},
//        {$project: {
//            _id: 0, "total_sale": 1}}


        // Previously It was:
        // 1: April
        //{invoice_date:    { $gte:  new Date("2015-03-31T18:30:00.000Z")}}
        //{invoice_date:    { $lte:  new Date("2015-04-30T18:30:00.000Z")}}
        // 2: May
//        {invoice_date:    { $gt:  new Date("2015-04-30T18:30:00.000Z")}},
//        {invoice_date:    { $lte:  new Date("2015-05-31T18:30:00.000Z")}}


        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved' },
                {ispoint_calculated : 0},
                {invoice_date:    { $gt:  new Date("2015-05-31T18:30:00.000Z")}},
                {invoice_date:    { $lte:  new Date("2015-06-30T18:30:00.000Z")}}

            ]
        }},
        {$unwind:  '$claim_details'},
        {$group : {     _id: {'is_free': '$claim_details.is_free',"status" : "$claim_details.status"},
            'total_sale' : {$sum: '$claim_details.amount'}}},
        {$project: {
            _id: 0, 'is_free':'$_id.is_free',status:'$_id.status', "total_sale": 1}},
        {$match: {$and :
            [
                {is_free: 0},
                {status: "verified"}

            ]
        }}


    ];
    return this.aggregate (pipeline)
    .exec();// Should return a Promise
}


sales_registrySchema.statics.get_sales_bu_monthtwo = function(){
    var pipeline =[
//        {$match: {$and :
//            [
//                {active:1},
//                {status : 'Verified & Approved' },
//                {ispoint_calculated : 0} ,
//                {invoice_date:    { $gt:   new Date("2015-04-30T18:30:00.000Z")}},
//                {invoice_date:    { $lte:  new Date("2015-05-31T18:30:00.000Z")}}
//            ]
//
//        }},
//        {$group : {     _id: {'total_sale': '$total_sale'},
//            'total_sale' : {$sum: '$verified_amount'}}},
//        {$project: {
//            _id: 0, "total_sale": 1}}



        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved' },
                {ispoint_calculated : 0},
                {invoice_date:    { $gt:  new Date("2015-06-30T18:30:00.000Z")}},
                {invoice_date:    { $lte:  new Date("2015-07-31T18:30:00.000Z")}}

            ]
        }},
        {$unwind:  '$claim_details'},
        {$group : {     _id: {'is_free': '$claim_details.is_free',"status" : "$claim_details.status"},
            'total_sale' : {$sum: '$claim_details.amount'}}},
        {$project: {
            _id: 0, 'is_free':'$_id.is_free',status:'$_id.status', "total_sale": 1}},
        {$match: {$and :
            [
                {is_free: 0},
                {status: "verified"}

            ]
        }}



    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}


sales_registrySchema.statics.get_sales_bu_monththree = function(){
    var pipeline =[
//        {$match: {$and :
//            [
//                {active:1},
//                {status : 'Verified & Approved' },
//                {invoice_date:    { $gt: new Date( "2015-05-31T18:30:00.000Z" )}},
//                {invoice_date:    { $lte: new Date( "2015-06-30T18:30:00.000Z" )}},
//                {ispoint_calculated : 0}
//            ]
//
//        }},
//        {$group : {     _id: {'total_sale': '$total_sale'},
//            'total_sale' : {$sum: '$verified_amount'}}},
//        {$project: {
//            _id: 0, "total_sale": 1}}


        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved' },
                {ispoint_calculated : 0},
                {invoice_date:    { $gt:  new Date("2015-07-31T18:30:00.000Z")}},
                {invoice_date:    { $lte:  new Date("2015-08-31T18:30:00.000Z")}}

            ]
        }},
        {$unwind:  '$claim_details'},
        {$group : {     _id: {'is_free': '$claim_details.is_free',"status" : "$claim_details.status"},
            'total_sale' : {$sum: '$claim_details.amount'}}},
        {$project: {
            _id: 0, 'is_free':'$_id.is_free',status:'$_id.status', "total_sale": 1}},
        {$match: {$and :
            [
                {is_free: 0},
                {status: "verified"}

            ]
        }}



    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_current_month_total_amount = function(){

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var first_date_format = firstDay.getFullYear() + '-' + ('0' + (firstDay.getMonth()+1)).slice(-2) +'-'+ ('0' + firstDay.getDate()).slice(-2);

    var last_date_format = lastDay.getFullYear() +'-'+ ('0' + (lastDay.getMonth()+1)).slice(-2) +'-'+ ('0' + lastDay.getDate()).slice(-2);

    console.log(first_date_format , last_date_format);

    var pipeline =[

        {$match: {$and :
            [
                {active:1},
                {status : 'Verified & Approved' },
                {ispoint_calculated : 0},
                {invoice_date:    { $gt:  new Date(first_date_format+"T18:30:00.000Z")}},
                {invoice_date:    { $lte:  new Date(last_date_format+"T18:30:00.000Z")}}

            ]
        }},
        {$unwind:  '$claim_details'},
        {$group : {     _id: {'is_free': '$claim_details.is_free',"status" : "$claim_details.status",'retailer' : '$retailer'},
            'total_sale' : {$sum: '$claim_details.amount'}}},
        {$project: {
            _id: 0, 'is_free':'$_id.is_free',status:'$_id.status','retailer' : '$_id.retailer' ,"total_sale": 1}},
        {$match: {$and :
            [
                {is_free: 0},
                {status: "verified"}

            ]
        }}


    ];

    console.log(pipeline);
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



sales_registrySchema.set('collection', 'sales_registry')
//module.exports = mongoose.model('category', catSchema);

module.exports = mongo.get_mongoose_connection().model('sales_registry', sales_registrySchema);